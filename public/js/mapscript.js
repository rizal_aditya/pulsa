//Home Depan
var arrMarker = [];
var map;
var tooltip;

function mapscript(){
$(document).ready(function() {
	init();
	getLokasiAll($(this).val());
	getPrint($(this).attr('data'),$(this).val());

	$("#all").click(function(){
		getLokasiAll($(this).val());
		getListStatus($(this).val());
		getPrint($(this).attr('data'),$(this).val());
		$(".kat_text").html($(this).attr('alt'));
		$(".menuKategori").show();
		$("#opt").html($(this).val());
		$("#welcome").hide();
		$("#list").show();
		$(".semua").show();
		$(".kcmitra").hide();
		 $(".kmitra").hide();


	});	

	$("#cmitra").click(function(){
		getLokasiAll($(this).val());
		getListStatus($(this).val());
		getPrint($(this).attr('data'),$(this).val());
		$(".kat_text").html($(this).attr('alt'));
		$(".menuKategori").show();
		$("#welcome").hide();
		$("#list").show();
		$(".semua").hide();
		$(".kcmitra").show();
		 $(".kmitra").hide();
	});

	$("#mitra").click(function(){
		getLokasiAll($(this).val());
		getListStatus($(this).val());
		getPrint($(this).attr('data'),$(this).val());
		$(".kat_text").html($(this).attr('alt'));
		$(".menuKategori").show();
		$("#welcome").hide();
		$("#list").show();
		$(".semua").hide();
		$(".kcmitra").hide();
		$(".kmitra").show();
		
	});

	$(".optkategori").click(function(){
		//console.log($(this).attr('data-img'));
		$("#kat-img").attr('src',$(this).attr('data-img'));
		$(".kat_text").html($(this).attr('alt'));
		$("#welcome").hide();
		$("#list").show();
		getPrint($(this).attr('data'),$(this).attr('status'));
		getLokasi($(this).attr('data'),$(this).attr('status'));
		getListLokasi($(this).attr('data'),$(this).attr('status'));

	});



	$("#searchform").submit(function(){
		var txt = $("#cari").val();
		getCari(txt);
		return false;
	})
	
}); 
function init(){
	var myLatlng = new google.maps.LatLng(-7.788554, 110.369339);
	var myOptions = {
	zoom: 11,
	center: myLatlng,
	mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);  
	var styleOptions = {
            name: "Dummy Style"
        };
        
    var MAP_STYLE = [
	    {
	        featureType: "road",
	        elementType: "all",
	        stylers: [
	            { visibility: "on" }
	        ]
	    }
	];
    var mapType = new google.maps.StyledMapType(MAP_STYLE, styleOptions);
    map.mapTypes.set("Dummy Style", mapType);
    map.setMapTypeId("Dummy Style");
  
}



function getLokasiAll(e){
  	$.ajax({
      type:"GET",
      dataType:"json",
      url: "json?mode=lokasiAll&status="+e,
      cache: false,
      success: function(data){
      	//alert('tes');
		clearMarker();
        buildMarker(data);
        console.log(data);
      },
      error: function (data) {
        alert("Gagala");
        
      }
   	});
}

function getLokasi(e,f){
  	$.ajax({
      type:"GET",
      dataType:"json",
      url: "json?mode=lokasi&id="+e+"&status="+f,
      cache: false,
      success: function(data){
      	//alert(e);
		clearMarker();
        buildMarker(data);
        console.log(data);
      },
      error: function (data) {
        alert("Gagals");
        
      }
   	});
} 

function getListStatus(e){
	$("#detail_list").html('<div class="loading"><img src="http://aisautocare.co.id/css/images/loading.gif" /><br />Loading...</div>');
  	$.ajax({
       type:"GET",
      url: "json?mode=list_status&status="+e,
      cache: false,
      success: function(data){
		$("#detail_list").html(data);
		//alert('tes');
      }
   	});
} 
function getListLokasi(e,f){
	$("#detail_list").html('<div class="loading"><img src="http://aisautocare.co.id/css/images/loading.gif" /><br />Loading...</div>');
  	$.ajax({
       type:"GET",
      url: "json?mode=list_lokasi&id="+e+"&status="+f,
      cache: false,
      success: function(data){
		$("#detail_list").html(data);
		//alert('tes');
      }
   	});
} 
function getCari(e){
	$.ajax({
      type:"GET",
      dataType:"json",
      url: "json?mode=cari&name="+e,
      data: {id:e},
      cache: false,
      success: function(data){
		clearMarker();
        buildMarker(data);
        
      }
   	});
}

function onLokasiClick(e){
	clearInfoKampung();
  	for(var i in arrMarker) {
  		var m =	arrMarker[i];
  		if(m.data.id == e.id){
  			m.infowindow.open(map,m);
  		}
  	}	
}


function buildMarker(data){
	for(var i in data) {
		var markerOptions = {
			map : map,
			position: new google.maps.LatLng(data[i].lat, data[i].lng),
			title: data[i].nama,
			icon:  '/'+data[i].category,
			tooltip: data[i].nama
		};
		var marker = new google.maps.Marker(markerOptions);
		marker.data = data[i];
		marker.infowindow = new google.maps.InfoWindow({content:data[i].nama});
		marker.infowindow.content  = '<div id="infowindow">'+ 
										'<div class="title">'+ 
											'<div class="icon" style="background:url(admin/upload/'+data[i].icon+') no-repeat center;"></div>'+ 
											'<div class="text">'+data[i].nama+'</div>'+ 
										'</div> <table> <tbody><tr><td width="70"><b>Alamat</b></td><td width="10">:</td><td> '+data[i].almt+'</td></tr>'+
										'<tr><td width="70"><b>Kecamatan</b></td><td width="10">:</td><td>'+data[i].kecamatan+'</td></tr>'+
										'<tr><td width="70"><b>kabupaten</b></td><td width="10">:</td><td>'+data[i].kabupaten+'</td></tr>'+
										'</tbody></table>'+
										'<br><a href="" target="_blank">Detail Info &amp; Rute Perjalanan </a>'+
										'</div>';
		google.maps.event.addListener(marker, 'click', function(){
			onLokasiClick(this.data);
		});
		marker.tp = new Tooltip({map: map}, marker);
		marker.tp.bindTo("text", marker, "tooltip");

        google.maps.event.addListener(marker, 'mouseover', function() {
        	showTooltip(this.data);
        });
  	
        google.maps.event.addListener(marker, 'mouseout', function() {
            hideTooltip(this.data);
        });
		arrMarker.push(marker);
	}
}

function getPrint(e,f){
	
  	$.ajax({
      type:"GET",
      url: "json?mode=print&id="+e+"&status="+f,
      cache: false,
      success: function(data){
		$(".printe").html(data);
      }
   	});
} 

function showTooltip(e){
	for(var i in arrMarker) {
  		var m =	arrMarker[i];
  		if(m.data.id == e.id){
  			//console.log(m);
  			 m.tp.addTip();
	         m.tp.getPos2(m.getPosition());
  		}
  	}	
}
function hideTooltip(e){
	for(var i in arrMarker) {
  		var m =	arrMarker[i];
  		if(m.data.id == e.id){
  			//console.log(m);
  			 m.tp.removeTip();
	        // m.tp.getPos2(m.getPosition());
  		}
  	}	
}

function clearMarker() { 
	for(var i in arrMarker) {
		if(!!arrMarker[i].infoWindow) arrMarker[i].infoWindow.close();
		arrMarker[i].setMap(null);
	}
	arrMarker.length = 0;
}
function clearInfoKampung() {
	for(var j in arrMarker) {
		arrMarker[j].infowindow.close();
	}
}
function onListLokasiClick(e){
	clearInfoKampung();
  	for(var i in arrMarker) {
  		var m =	arrMarker[i];
  		if(m.data.id == e){
  			m.infowindow.open(map,m);
  		}
  	}	
}

}






<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use App\Models\Branch;
use Auth;
use App\Role;
/**
 * Class PostCriteria
 * @package namespace App\Criteria;
 */

   

class MenuCriteria implements CriteriaInterface
{

     public $type;
     public function __construct($type)
     {
        $this->type = $type;
        
     }
   
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
         $model = $model->where('role_id', Role::where('name', $this->type)->first()->id)->orderBy('id','desc');
         return $model;
    }
}

<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Auth;

/**
 * Class PostUserCriteria
 * @package namespace App\Criteria;
 */

class HistoryCriteria implements CriteriaInterface
{

     
     public function __construct()
     {
      
     }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {

        $user = Auth::user();
        $model = $model->where(['user_id'=> $user->id])->orderBy('id','desc'); 
        return $model;
    }
}

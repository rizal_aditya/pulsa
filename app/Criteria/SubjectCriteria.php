<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use App\Models\Branch;
use Auth;

/**
 * Class PostUserCriteria
 * @package namespace App\Criteria;
 */
class SubjectCriteria implements CriteriaInterface
{

     private $type;
     public function __construct($type)
     {
        $this->type      = $type;
     }
    
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */

    public function apply($model, RepositoryInterface $repository)
    {
        
        $userid = Auth::user();
        if($userid->hasRole('admin'))
        {    
             $model = $model->where(['type'=>$this->type])->orderBy('id','desc');   
         
        }else if($userid->hasRole('user')){
            
            $model = $model->where(['type'=>$this->type,'branch_id'=> Branch::where('user_id',$userid->id)->first()->id])->orderBy('id','desc');   
        }

        return $model;
    }
}

<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use App\Models\Branch;
use Auth;
/**
 * Class PostCriteria
 * @package namespace App\Criteria;
 */

   

class ConfirmationCriteria implements CriteriaInterface
{
    private $branch_id;
    public function __construct($branch_id)
     {
        
        $this->branch_id = $branch_id;
     }
   
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {

         $userid = Auth::user();
            if($userid->hasRole('admin'))
            {    
                $model = $model->orderBy('id','desc');

             }else if($userid->hasRole('user')){

                  $model = $model->where(['branch_id' => $this->branch_id])->orderBy('id','decs');  
            }

         return $model;
    }
}

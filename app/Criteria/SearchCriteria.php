<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Auth;

/**
 * Class PostUserCriteria
 * @package namespace App\Criteria;
 */

class SearchCriteria implements CriteriaInterface
{

     
     public function __construct($type,$tanggal)
     {
        $this->type = $type;
        $this->tanggal = $tanggal;
     }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {

        $model = $model->where('creagted_at', 'like', $this->tanggal.'%');    
        return $model;
    }
}

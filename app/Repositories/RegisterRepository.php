<?php

namespace App\Repositories;

use App\Models\Register;
use InfyOm\Generator\Common\BaseRepository;

class RegisterRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
       
        'name',
        'owner',
        'manager',
        'phone_manager',
        'email',
        'phone',
        'hp',
        'address',
        'photo',
        'whatapps',
        'bbm',
        'instagram',
        'bussines_primary',
        'bussines_secondary',
        'etc',
        'sdm',
        'latitude',
        'longitude',
         'kecamatan',
        'kabupaten',
        'essay1',
        'essay2',
        'pilihan1',
        'pilihan2',
        'pilihan3',
        'termahal',
        'termurah',
        'type',
        'status',
        'essay3'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Register::class;
    }
}

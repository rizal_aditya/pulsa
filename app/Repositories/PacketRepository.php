<?php

namespace App\Repositories;

use App\Models\Packet;
use InfyOm\Generator\Common\BaseRepository;

class PacketRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'denom',
        'price',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Packet::class;
    }
}

<?php

namespace App\Repositories;

use App\Models\Deposite;
use InfyOm\Generator\Common\BaseRepository;

class DepositeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'packet_id',
        'user_id',
        'denom',
        'price',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Deposite::class;
    }
}

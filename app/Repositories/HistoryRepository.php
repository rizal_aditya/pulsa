<?php

namespace App\Repositories;

use App\Models\History;
use InfyOm\Generator\Common\BaseRepository;

class HistoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'transaction_id',
        'user_id',
        'pulsa_id',
        'price',
        'phone',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return History::class;
    }
}

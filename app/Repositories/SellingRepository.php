<?php

namespace App\Repositories;

use App\Models\Selling;
use InfyOm\Generator\Common\BaseRepository;

class SellingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'price',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Selling::class;
    }
}

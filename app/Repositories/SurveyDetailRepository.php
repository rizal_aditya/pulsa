<?php

namespace App\Repositories;

use App\Models\SurveyDetail;
use InfyOm\Generator\Common\BaseRepository;

class SurveyDetailRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'survey_id',
        'user_id',
        'essay',
        'selection',
        'status',
        'type'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SurveyDetail::class;
    }
}

<?php

namespace App\Repositories;

use App\Models\Postfile;
use InfyOm\Generator\Common\BaseRepository;

class PostfileRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'post_id',
        'branch_id',
        'type',
        'name',
        'content',
        'file',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Postfile::class;
    }
}

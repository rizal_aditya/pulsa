<?php

namespace App\Repositories;

use App\Models\User;
use InfyOm\Generator\Common\BaseRepository;

class UserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'client_id',
        'name',
        'email',
        'password',
        'phone',
        'address',
        'photo',
        'deposite',
        'location',
        'code_agen',
        'owner',
        'security',
        'status',
        'type'
        
       
       
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }
}
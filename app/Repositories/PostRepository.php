<?php

namespace App\Repositories;

use App\Models\Post;
use InfyOm\Generator\Common\BaseRepository;

class PostRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'type',
        'title',
        'content',
        'start_date',
        'end_date',
        'position_img',
        'broadcast',
        'broadcast_type',
        'class_room_id',
        'parent',
        'color',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Post::class;
    }
}

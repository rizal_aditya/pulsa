<?php

namespace App\Repositories;

use App\Models\Join;
use InfyOm\Generator\Common\BaseRepository;

class JoinRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
       
        'name',
        'owner',
        'manager',
        'phone_manager',
        'email',
        'phone',
        'hp',
        'address',
        'photo',
        'whatapps',
        'bbm',
        'instagram',
        'bussines_primary',
        'bussines_secondary',
        'etc',
        'sdm',
        'latitude',
        'longitude',
        'essay1',
        'essay2',
        'pilihan1',
        'pilihan2',
        'pilihan3',
        'termahal',
        'termurah',
        'type',
        'status',
        'essay3'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Join::class;
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function (Request $request) {
   return view('login');
    //return redirect('index');
   
});


/*
|--------------------------------------------------------------------------
| API routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'api', 'namespace' => 'API'], function () {
    Route::group(['prefix' => 'v1'], function () {
    	Route::post('auth', 'AuthAPIController@authenticate');
        Route::post('forgotPassword', 'AuthAPIController@forgot');
        Route::post('user/register', 'UserAPIController@register');
    	Route::group(['middleware' => ['jwt.auth']], function () {
            require config('infyom.laravel_generator.path.api_routes');
        });
    });
});



Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('logout', 'Auth\AuthController@logout');

// Registration Routes...
// Route::get('register', 'Auth\AuthController@getRegister');
// Route::post('register', 'Auth\AuthController@postRegister');

// Password Reset Routes...
Route::get('password/reset', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::group(['middleware' => ['auth']], function () {

    Route::get('fileupload', [
        'as' => 'fileupload',
        'uses' => 'Barryvdh\Elfinder\ElfinderController@showPopup',
    ]);

    Route::group(['middleware' => ['role:admin']], function () {
    
    });
    // tampilkan sesudah login
	Route::get('home', 'HomeController@index');
 
    
    Route::resource('user/login', 'UserController@login');
    Route::resource('user', 'UserController');


    
  
    Route::get('user/{id}/enable','UserController@enable');
    Route::get('user/{id}/disable','UserController@disable');
    Route::get('user/{id}/notification','UserController@notification');
    Route::get('user/deposite','UserController@deposite');

    Route::get('deposite/enable','DepositeController@enable');
    Route::get('deposite/disable','DepositeController@disable');
     Route::resource('role', 'RoleController');
    
   
    Route::resource('client', 'ClientController');
  
  
    Route::resource('device', 'DeviceController');
  

    Route::resource('menu', 'MenuController');
 

    
    
});




Route::resource('client', 'ClientController');
Route::resource('transaction', 'TransactionController');
Route::resource('pulsa', 'VoucherUserController');
Route::resource('selling', 'SellingController');
Route::resource('purchase', 'PurchaseController');
Route::resource('history', 'HistoryController');
Route::resource('packet', 'PacketController');
Route::resource('deposite', 'DepositeController');


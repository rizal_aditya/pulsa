<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where all API routes are defined.
|
*/

/* --- User API --- */

Route::get('user/myProfile', 'UserAPIController@myProfile');
Route::get('user/cekSaldo', 'UserAPIController@cekSaldo');
Route::get('user/cekSecurity', 'UserAPIController@cekSecurity');
Route::put('user/security', 'UserAPIController@security');
Route::put('user/updatePassword', 'UserAPIController@updatePassword');
Route::post('transaction/store', 'TransactionAPIController@store');
Route::put('user/updateProfile', 'UserAPIController@updateProfile');

/* --- Device API --- */

Route::post('device/check', 'DeviceAPIController@check');

Route::post('update/check', 'UpdateAPIController@check');

/* --- Client API --- */

Route::get('client', 'ClientAPIController@index');

Route::group(['middleware' => ['role:admin']], function () {

	Route::resource('user', 'UserAPIController');
	
	

	
	Route::resource('device', 'DeviceAPIController');
	
	
	
	Route::resource('menu', 'MenuAPIController');
	

});



Route::resource('client', 'ClientAPIController');
Route::resource('transaction', 'TransactionAPIController');

Route::resource('selling', 'SellingAPIController');
Route::resource('purchase', 'PurchaseAPIController');
Route::resource('history', 'HistoryAPIController');
Route::resource('packet', 'PacketAPIController');
Route::resource('deposite', 'DepositeAPIController');
Route::post('deposite/store', 'DepositeAPIController@store');


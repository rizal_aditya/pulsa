<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateDepositeRequest;
use App\Http\Requests\UpdateDepositeRequest;
use App\Repositories\DepositeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Auth;
use App\User;

class DepositeController extends AppBaseController
{
    /** @var  DepositeRepository */
    private $depositeRepository;

    public function __construct(DepositeRepository $depositeRepo)
    {
        $this->depositeRepository = $depositeRepo;
    }

    /**
     * Display a listing of the Deposite.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->depositeRepository->pushCriteria(new RequestCriteria($request));
        $deposite = $this->depositeRepository->all();

        return view('deposite.index')
            ->with('deposite', $deposite);
    }

    /**
     * Show the form for creating a new Deposite.
     *
     * @return Response
     */
    public function create()
    {
        return view('deposite.create');
    }

    /**
     * Store a newly created Deposite in storage.
     *
     * @param CreateDepositeRequest $request
     *
     * @return Response
     */
    public function store(CreateDepositeRequest $request)
    {
        $input = $request->all();

        $deposite = $this->depositeRepository->create($input);

        Flash::success('Deposite saved successfully.');

        return redirect(route('deposite.index'));
    }

    /**
     * Display the specified Deposite.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $deposite = $this->depositeRepository->findWithoutFail($id);

        if (empty($deposite)) {
            Flash::error('Deposite not found');

            return redirect(route('deposite.index'));
        }

        return view('deposite.show')->with('deposite', $deposite);
    }

     public function enable($id,Request $request)
    {
        $deposite = $this->depositeRepository->findWithoutFail($id);
        
       
        if (empty($deposite)) {
            Flash::error('Deposite not found');

           return redirect(url('deposite'));

        }

        $status['status'] = '1';
        $saldo = $deposite->denom;
        $user = $deposite->user_id;
        $deposite = $this->depositeRepository->update($status, $id);

        $us = User::where('id', $user)->update(['deposite' => $saldo]);

        Flash::success('Deposite updated successfully.');
        return redirect(url('deposite'));
        
    }


    public function disable($id,Request $request)
    {
        $deposite = $this->depositeRepository->findWithoutFail($id);
        
       
        if (empty($deposite)) {
            Flash::error('Deposite not found');

           return redirect(url('deposite'));

        }

       $status['status'] = '0';
       $saldo = '0';
       $user = $deposite->user_id;
       $deposite = $this->depositeRepository->update($status, $id);
       
       $us = User::where('id', $user)->update(['deposite' => $saldo]);
      

        Flash::success('Deposite updated successfully.');
        return redirect(url('deposite'));
        
    }


    /**
     * Show the form for editing the specified Deposite.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $deposite = $this->depositeRepository->findWithoutFail($id);

        if (empty($deposite)) {
            Flash::error('Deposite not found');

            return redirect(route('deposite.index'));
        }

        return view('deposite.edit')->with('deposite', $deposite);
    }

    /**
     * Update the specified Deposite in storage.
     *
     * @param  int              $id
     * @param UpdateDepositeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDepositeRequest $request)
    {
        $deposite = $this->depositeRepository->findWithoutFail($id);

        if (empty($deposite)) {
            Flash::error('Deposite not found');

            return redirect(route('deposite.index'));
        }

        $deposite = $this->depositeRepository->update($request->all(), $id);

        Flash::success('Deposite updated successfully.');

        return redirect(route('deposite.index'));
    }

    /**
     * Remove the specified Deposite from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $deposite = $this->depositeRepository->findWithoutFail($id);

        if (empty($deposite)) {
            Flash::error('Deposite not found');

            return redirect(route('deposite.index'));
        }

        $this->depositeRepository->delete($id);

        Flash::success('Deposite deleted successfully.');

        return redirect(route('deposite.index'));
    }
}

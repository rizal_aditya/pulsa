<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDepositeAPIRequest;
use App\Http\Requests\API\UpdateDepositeAPIRequest;
use App\Criteria\DepositeCriteria;
use App\Repositories\DepositeRepository;
use App\Repositories\PacketRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class DepositeController
 * @package App\Http\Controllers\API
 */

class DepositeAPIController extends AppBaseController
{
    /** @var  DepositeRepository */
    private $depositeRepository;
    private $packetRepository;
    public function __construct(DepositeRepository $depositeRepo,PacketRepository $packetRepo)
    {
        $this->depositeRepository = $depositeRepo;
        $this->packetRepository = $packetRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/deposite",
     *      summary="Get a listing of the Deposite.",
     *      tags={"Deposite"},
     *      description="Get all Deposite",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Deposite")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->depositeRepository->pushCriteria(new DepositeCriteria($request));
        $this->depositeRepository->pushCriteria(new RequestCriteria($request));
        $this->depositeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $deposite = $this->depositeRepository->all();

        return $this->sendResponse($deposite->toArray(), 'History Deposite successfully');
    }

    /**
     * @param CreateDepositeAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/deposite",
     *      summary="Store a newly created Deposite in storage",
     *      tags={"Deposite"},
     *      description="Store Deposite",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Deposite that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Deposite")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Deposite"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateDepositeAPIRequest $request)
    {
        $input = $request->all();
        $user = Auth::user();
        $packet = $this->packetRepository->find($request->packet_id);
        $input['packet_id'] = $request->packet_id;
        $input['user_id'] = $user->id;
        $input['denom'] = $packet->denom;
        $input['price'] = $packet->price;
        $input['status'] = '0';
        $deposite = $this->depositeRepository->create($input);

       
        //$saldo =  User::where('id', $user->id)->update(['deposite' => $packet[0]->denom]);



        return $this->sendResponse($deposite->toArray(), 'Deposite saved successfully');
    }




    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/deposite/{id}",
     *      summary="Display the specified Deposite",
     *      tags={"Deposite"},
     *      description="Get Deposite",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Deposite",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Deposite"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Deposite $deposite */
        $deposite = $this->depositeRepository->find($id);

        if (empty($deposite)) {
            return Response::json(ResponseUtil::makeError('Deposite not found'), 404);
        }

        return $this->sendResponse($deposite->toArray(), 'Deposite retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateDepositeAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/deposite/{id}",
     *      summary="Update the specified Deposite in storage",
     *      tags={"Deposite"},
     *      description="Update Deposite",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Deposite",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Deposite that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Deposite")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Deposite"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateDepositeAPIRequest $request)
    {
        $input = $request->all();

        /** @var Deposite $deposite */
        $deposite = $this->depositeRepository->find($id);

        if (empty($deposite)) {
            return Response::json(ResponseUtil::makeError('Deposite not found'), 404);
        }

        $deposite = $this->depositeRepository->update($input, $id);

        return $this->sendResponse($deposite->toArray(), 'Deposite updated successfully');
    }


   



    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/deposite/{id}",
     *      summary="Remove the specified Deposite from storage",
     *      tags={"Deposite"},
     *      description="Delete Deposite",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Deposite",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Deposite $deposite */
        $deposite = $this->depositeRepository->find($id);

        if (empty($deposite)) {
            return Response::json(ResponseUtil::makeError('Deposite not found'), 404);
        }

        $deposite->delete();

        return $this->sendResponse($id, 'Deposite deleted successfully');
    }
}

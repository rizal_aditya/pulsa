<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateUserAPIRequest;
use App\Http\Requests\API\UpdateUserAPIRequest;
use App\Http\Requests\API\UpdatePasswordUserAPIRequest;
use App\Http\Requests\API\UpdateSecurityUserAPIRequest;
use App\User;
use App\Role;
use App\RoleUser;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Auth;
use Hash;
use Intervention\Image\ImageManagerStatic as Image;
use Config;
use App\Models\Client;
use DB;

/**
 * Class userController
 * @package App\Http\Controllers\API
 */

class UserAPIController extends AppBaseController
{
    /** @var  userRepository */
    use AuthenticatesAndRegistersUsers;
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }


    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/user",
     *      summary="Get a listing of the Asset.",
     *      tags={"User"},
     *      description="Get all Asset",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/User")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->userRepository->pushCriteria(new RequestCriteria($request));
        $this->userRepository->pushCriteria(new LimitOffsetCriteria($request));
        $user = $this->userRepository->all();

        return $this->sendResponse($user->toArray(), 'user retrieved successfully');
    }

    public function store(CreateUserAPIRequest $request)
    {

        $input = $request->all();

        $user = $this->create($input);

        return $this->sendResponse($user->toArray(), 'user saved successfully');
    }

    protected function create(array $data)
    {

        $data['password'] =  Hash::make($data['password']);
        $user = User::create($data);

        /*if ($user) {
            $role = Role::where('name', $data['role'])->first();
            $roleUser = new RoleUser;
            $roleUser->user_id = $user->id;
            $roleUser->role_id = $role->id;
        }*/

        return $user;
    }

    public function show($id)
    {
        /** @var user $user */
        $user = $this->userRepository->find($id);

        if (empty($user)) {;
            return Response::json(ResponseUtil::makeError('user not found'), 404);
        }

        return $this->sendResponse($user->toArray(), 'user retrieved successfully');
    }

    public function update($id, UpdateUserAPIRequest $request)
    {
        $input = $request->all();

        /** @var user $user */
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return Response::json(ResponseUtil::makeError('user not found'), 404);
        }

        $user = $this->userRepository->update($input, $id);

        return $this->sendResponse($user->toArray(), 'user updated successfully');
    }

    public function destroy($id)
    {
        /** @var user $user */
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return Response::json(ResponseUtil::makeError('user not found'), 404);
        }

        $user->delete();

        return $this->sendResponse($id, 'user deleted successfully');
    }


    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/register",
     *      summary="Get a listing of the Asset.",
     *      tags={"User"},
     *      description="Get all Asset",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/User")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function register(CreateUserAPIRequest $request)
    {

        $input = $request->all();
        if (!$request->has('type'))
            $input['type'] = 'mitra';
            $input['status'] = '0';
        if (!$request->has('photo') || $input['photo']=='') {
            $input['photo'] = Config::get('app.photo_default');
        }

    
      
        $role_id = Role::where('name', 'admin')->first()->id;
        $client = Client::where('role_id',$role_id)->first()->id;
        $input['client_id'] = $client;
        $user = $this->create($input);
        $code_agen = "LY-".$user->id;


         DB::table('users')->where('id', $user->id)->update(['code_agen' => $code_agen]); 

        if (!$request->has('type'))
        {    
            $role = Role::where('name', 'user')->first();
           
       }else{
            $role = Role::where('name', $input['type'])->first();
        }
            
        if (!empty($role)) {

            $roleUser = new RoleUser;
            $roleUser->user_id = $user->id;
            $roleUser->role_id = $role->id;
            $roleUser->save();

        }

        return $this->sendResponse($user->toArray(), 'user saved successfully');
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/myProfile",
     *      summary="Get a listing of the Asset.",
     *      tags={"User"},
     *      description="Get all Asset",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(
      *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="client_id",
 *          description="client_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="birthday",
 *          description="birthday",
 *          type="date"
 *      ),
 *      @SWG\Property(
 *          property="religion",
 *          description="religion",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="phone",
 *          description="phone",
 *          type="string"
 *      ),
  *      @SWG\Property(
 *          property="address",
 *          description="address",
 *          type="text"
 *      ),
  *      @SWG\Property(
 *          property="photo",
 *          description="photo",
 *          type="string"
 *      ),
  *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
                        )
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function myProfile()
    {
        $id = Auth::user()->id;
        $user = DB::table('users')->select('id','code_agen','name','owner','email','location','address','phone','photo','type','status','remember_token','created_at','updated_at')->where('id',$id)->get();

        if (empty($user)) {;
            return Response::json(ResponseUtil::makeError('user not found'), 404);
        }

        return $this->sendResponse($user, 'user retrieved successfully');
    }


    public function cekSaldo()
    {
        $id = Auth::user()->id;
    
        $users = DB::table('users')->select('id','name', 'deposite','status')->where('id',$id)->get();

        if (empty($users)) {;
            return Response::json(ResponseUtil::makeError('user not found'), 404);
        }

        return $this->sendResponse($users, 'user retrieved successfully');
    }

    public function cekSecurity()
    {
        $id = Auth::user()->id;
        $users = DB::table('users')->select('id','name', 'security','status')->where('id',$id)->get();

        if (empty($users)) {;
            return Response::json(ResponseUtil::makeError('user not found'), 404);
        }

        return $this->sendResponse($users, 'user retrieved successfully');
    }

    public function security(UpdateSecurityUserAPIRequest $request)
    {
        $input = $request->only('password');

        $id = Auth::user()->id;
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return Response::json(ResponseUtil::makeError('user not found'), 404);
        }

        if (!Auth::validate(array('email' => Auth::user()->email, 'password' => $input['password'], 'client_id' => $user->client_id)))
        {
             $secure = sprintf("%04d", mt_rand(1, 9999));
             $data = "LY-";
             $security = $data.$secure;
             $us = User::where('id', $id)->update(['security' => $security]);
             $users = DB::table('users')->select('id','client_id','name','security','status')->where('id',$id)->get();
             return $this->sendResponse($users, 'security updated successfully');
        }

             $secure = sprintf("%04d", mt_rand(1, 9999));
             $data = "LY-";
             $security = $data.$secure;
             $us = User::where('id', $id)->update(['security' => $security]);
              $users = DB::table('users')->select('id','client_id','name','security','status')->where('id',$id)->get();
             return $this->sendResponse($users, 'security updated successfully');
        
    }


        /**
     * @param CreateUserAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/updatePassword",
     *      summary="Update password",
     *      tags={"User"},
     *      description="Update password",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="User Password that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/User")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Transaction"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function updateProfile(UpdateUserAPIRequest $request)
    {
        $input = $request->except('email', 'password', 'status', 'create_at', 'update_at');

        /** @var user $user */
        $id = Auth::user()->id;
        $user = $this->userRepository->find($id);
        
        $date = date_create($request->birthday);
        $input['date'] = date_format($date,"m-d");

        if (empty($user)) {
            return Response::json(ResponseUtil::makeError('user not found'), 404);
        }

        if ($request->has('photo')) {
            $fileFormat = ".jpg";
            $userPath = base64_encode(Auth::user()->email);
            $fileName = base64_encode(Auth::user()->email . time());
            $dir = Config::get('elfinder.dir')[0];
            $fullPath = $dir."/".$userPath."/".$fileName.$fileFormat;
            if ($image = base64_decode($input['photo'], true)) {
                $img = Image::make($image)->save($fullPath, 60);
                $input['photo'] = $fullPath;
            } else {
                return Response::json(ResponseUtil::makeError('Image not valid'), 404);
            }
        }

        $user = $this->userRepository->update($input, $id);

        return $this->sendResponse($user->toArray(), 'user updated successfully');
    }


    public function updatePassword(UpdatePasswordUserAPIRequest $request)
    {
        $input = $request->only('password', 'password_new');

        $id = Auth::user()->id;
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return Response::json(ResponseUtil::makeError('user not found'), 404);
        }

        if (!Auth::validate(array('email' => Auth::user()->email, 'password' => $input['password'], 'client_id' => $user->client_id)))
        {
            return Response::json(ResponseUtil::makeError('old password is incorrect'), 403);
        }

        $input['password'] = bcrypt($input['password_new']);

        $user = $this->userRepository->update($input, $id);

        return $this->sendResponse($user->toArray(), 'password updated successfully');
    }

}

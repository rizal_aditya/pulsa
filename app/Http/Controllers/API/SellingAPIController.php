<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSellingAPIRequest;
use App\Http\Requests\API\UpdateSellingAPIRequest;
use App\Models\Selling;
use App\Repositories\SellingRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class SellingController
 * @package App\Http\Controllers\API
 */

class SellingAPIController extends AppBaseController
{
    /** @var  SellingRepository */
    private $sellingRepository;

    public function __construct(SellingRepository $sellingRepo)
    {
        $this->sellingRepository = $sellingRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/selling",
     *      summary="Get a listing of the Selling.",
     *      tags={"Selling"},
     *      description="Get all Selling",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Selling")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->sellingRepository->pushCriteria(new RequestCriteria($request));
        $this->sellingRepository->pushCriteria(new LimitOffsetCriteria($request));
        $selling = $this->sellingRepository->all();

        return $this->sendResponse($selling->toArray(), 'Selling retrieved successfully');
    }

    /**
     * @param CreateSellingAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/selling",
     *      summary="Store a newly created Selling in storage",
     *      tags={"Selling"},
     *      description="Store Selling",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Selling that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Selling")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Selling"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSellingAPIRequest $request)
    {
        $input = $request->all();

        $selling = $this->sellingRepository->create($input);

        return $this->sendResponse($selling->toArray(), 'Selling saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/selling/{id}",
     *      summary="Display the specified Selling",
     *      tags={"Selling"},
     *      description="Get Selling",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Selling",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Selling"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Selling $selling */
        $selling = $this->sellingRepository->find($id);

        if (empty($selling)) {
            return Response::json(ResponseUtil::makeError('Selling not found'), 404);
        }

        return $this->sendResponse($selling->toArray(), 'Selling retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSellingAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/selling/{id}",
     *      summary="Update the specified Selling in storage",
     *      tags={"Selling"},
     *      description="Update Selling",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Selling",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Selling that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Selling")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Selling"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSellingAPIRequest $request)
    {
        $input = $request->all();

        /** @var Selling $selling */
        $selling = $this->sellingRepository->find($id);

        if (empty($selling)) {
            return Response::json(ResponseUtil::makeError('Selling not found'), 404);
        }

        $selling = $this->sellingRepository->update($input, $id);

        return $this->sendResponse($selling->toArray(), 'Selling updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/selling/{id}",
     *      summary="Remove the specified Selling from storage",
     *      tags={"Selling"},
     *      description="Delete Selling",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Selling",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Selling $selling */
        $selling = $this->sellingRepository->find($id);

        if (empty($selling)) {
            return Response::json(ResponseUtil::makeError('Selling not found'), 404);
        }

        $selling->delete();

        return $this->sendResponse($id, 'Selling deleted successfully');
    }
}

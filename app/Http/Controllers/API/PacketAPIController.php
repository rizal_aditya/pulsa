<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePacketAPIRequest;
use App\Http\Requests\API\UpdatePacketAPIRequest;
use App\Models\Packet;
use App\Repositories\PacketRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PacketController
 * @package App\Http\Controllers\API
 */

class PacketAPIController extends AppBaseController
{
    /** @var  PacketRepository */
    private $packetRepository;

    public function __construct(PacketRepository $packetRepo)
    {
        $this->packetRepository = $packetRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/packet",
     *      summary="Get a listing of the Packet.",
     *      tags={"Packet"},
     *      description="Get all Packet",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Packet")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->packetRepository->pushCriteria(new RequestCriteria($request));
        $this->packetRepository->pushCriteria(new LimitOffsetCriteria($request));
        $packet = $this->packetRepository->all();

        return $this->sendResponse($packet->toArray(), 'Packet retrieved successfully');
    }

    /**
     * @param CreatePacketAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/packet",
     *      summary="Store a newly created Packet in storage",
     *      tags={"Packet"},
     *      description="Store Packet",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Packet that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Packet")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Packet"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreatePacketAPIRequest $request)
    {
        $input = $request->all();

        $packet = $this->packetRepository->create($input);

        return $this->sendResponse($packet->toArray(), 'Packet saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/packet/{id}",
     *      summary="Display the specified Packet",
     *      tags={"Packet"},
     *      description="Get Packet",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Packet",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Packet"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Packet $packet */
        $packet = $this->packetRepository->find($id);

        if (empty($packet)) {
            return Response::json(ResponseUtil::makeError('Packet not found'), 404);
        }

        return $this->sendResponse($packet->toArray(), 'Packet retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePacketAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/packet/{id}",
     *      summary="Update the specified Packet in storage",
     *      tags={"Packet"},
     *      description="Update Packet",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Packet",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Packet that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Packet")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Packet"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatePacketAPIRequest $request)
    {
        $input = $request->all();

        /** @var Packet $packet */
        $packet = $this->packetRepository->find($id);

        if (empty($packet)) {
            return Response::json(ResponseUtil::makeError('Packet not found'), 404);
        }

        $packet = $this->packetRepository->update($input, $id);

        return $this->sendResponse($packet->toArray(), 'Packet updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/packet/{id}",
     *      summary="Remove the specified Packet from storage",
     *      tags={"Packet"},
     *      description="Delete Packet",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Packet",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Packet $packet */
        $packet = $this->packetRepository->find($id);

        if (empty($packet)) {
            return Response::json(ResponseUtil::makeError('Packet not found'), 404);
        }

        $packet->delete();

        return $this->sendResponse($id, 'Packet deleted successfully');
    }
}

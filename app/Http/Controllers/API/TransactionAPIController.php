<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTransactionAPIRequest;
use App\Http\Requests\API\UpdateTransactionAPIRequest;
use App\Repositories\TransactionRepository;
use App\Repositories\SellingRepository;
use App\Repositories\HistoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Auth;
use App\Models\Pulsa;
use App\Models\Client;
use App\Models\Transaction;
use App\Models\Purchase;
use App\User;
use App\Models\Selling;

/**
 * Class TransactionController
 * @package App\Http\Controllers\API
 */

class TransactionAPIController extends AppBaseController
{
    /** @var  TransactionRepository */
    private $transactionRepository;
    private $sellingRepository;
    private $historyRepository;

    public function __construct(TransactionRepository $transactionRepo,SellingRepository $sellingRepo,HistoryRepository $historyRepo)
    {
       $this->transactionRepository = $transactionRepo;
       $this->sellingRepository = $sellingRepo;
       $this->historyRepository = $historyRepo;
    }
    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/transaction",
     *      summary="Get a listing of the Transaction.",
     *      tags={"Transaction"},
     *      description="Get all Transaction",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Transaction")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->transactionRepository->pushCriteria(new RequestCriteria($request));
        $this->transactionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $transaction = $this->transactionRepository->all();

        return $this->sendResponse($transaction->toArray(), 'Transaction retrieved successfully');
    }

    /**
     * @param CreateTransactionAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/transaction",
     *      summary="Store a newly created Transaction in storage",
     *      tags={"Transaction"},
     *      description="Store Transaction",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Transaction that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Transaction")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Transaction"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateTransactionAPIRequest $request)
    {
         $input = $request->all();       
         $user = Auth::user();
         $client_id = Auth::user()->client_id;
         $client  = Client::where('id',$client_id)->first();
         $voucher = Pulsa::where('code',$request->code)->get();
        
         $harga_asal = $voucher[0]->purchase_id;
         $harga = Purchase::where('id',$harga_asal)->first()->price;
         $selling = $this->sellingRepository->findByField('id',$voucher[0]->selling_id)->first()->price;
         $input['user_id'] =  $user->id;
         $input['voucher_id'] = $voucher[0]->id;
         $input['purchase']   = $harga;
         $input['selling']   =  $selling;
         $input['client_id']  = $client_id;
         $admin = User::where('type','admin')->get();   
         
        if($admin[0]->deposite < $selling)   
        {
            return Response::json(ResponseUtil::makeError('Transaction not found!! Please contact admin PT Layana Computindo'), 404);
            
        }else{
            
         if($user->security == $request->security)
         { 
     
            if($user->deposite < $selling)
            {

                return Response::json(ResponseUtil::makeError('Transaction not found Balance insufficient'), 404);
        
            }else{  

             $transaction = $this->transactionRepository->create($input);
             $idtr = $transaction->id;

             $req_user = User::where('id', $user->id)->get();
             $code_agen = $req_user[0]->code_agen;
             $code_branch = $req_user[0]->location;
             $code_tr = $client->code_tr;
             $user_id = $client->id_user_era;

            $param1=date('hms');
            $param2=substr($request->phone,-4); 
            $param3=strrev($param2); 
            $param4 = $code_tr; 
            $hasilXOR=($param1."".$param2) ^ ($param3."".$param4); 
            $signature=base64_encode($hasilXOR);
              
            $url        = 'http://36.37.83.62:1982/cgi-bin/mc/xmlmc_id7.pl';
            $ch         = curl_init();
            curl_setopt( $ch, CURLOPT_URL, $url );
            curl_setopt( $ch, CURLOPT_POST, true );
            curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch, CURLOPT_POSTFIELDS, "<elevas>
            <command>TOPUP</command>
            <product>".$request->code."</product>
            <userid>".$user_id."</userid>
            <time>".$param1."</time>
            <msisdn>".$request->phone."</msisdn>
            <trxid>".$idtr."</trxid>
            <kode_store>".$code_agen."</kode_store>
            <kode_branch>".$code_branch."</kode_branch>
            <signature>".$signature."</signature>
            </elevas>" );

            $result = curl_exec($ch);
            curl_close($ch);

            // print_r($result);
            $tr =  Transaction::where('id', $idtr)->update(['result' => $result]);

           
           
             $his['transaction_id'] = $idtr;
             $his['user_id'] = $user->id;
             $his['pulsa_id'] =  $voucher[0]->id;
             $his['denom'] = $voucher[0]->denom;
             $his['price'] = $selling;
             $his['phone'] = $request->phone;
             $his['status'] = '0';
             $his['type'] = 'transaction';
             $history = $this->historyRepository->create($his);
            
             
             //kurangi saldo mitra
              $hitung1 = $user->deposite-$selling;
              $saldo1 =  User::where('id', $user->id)->update(['deposite' => $hitung1]);
              
              //kurangi saldo layana
            
              $hitung2 = $admin[0]->deposite-$harga;
              $saldo2 =  User::where('type','admin')->update(['deposite' => $hitung2]);
              
              
              return $this->sendResponse($history->toArray(), 'Transaction insert successfully');
              }
        
          }else{

             return Response::json(ResponseUtil::makeError('Transaction not found'), 404);
          }
      
      }
         

    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/transaction/{id}",
     *      summary="Display the specified Transaction",
     *      tags={"Transaction"},
     *      description="Get Transaction",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Transaction",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Transaction"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Transaction $transaction */
        $transaction = $this->transactionRepository->find($id);

        if (empty($transaction)) {
            return Response::json(ResponseUtil::makeError('Transaction not found'), 404);
        }

        return $this->sendResponse($transaction->toArray(), 'Transaction retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateTransactionAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/transaction/{id}",
     *      summary="Update the specified Transaction in storage",
     *      tags={"Transaction"},
     *      description="Update Transaction",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Transaction",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Transaction that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Transaction")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Transaction"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateTransactionAPIRequest $request)
    {
        $input = $request->all();

        /** @var Transaction $transaction */
        $transaction = $this->transactionRepository->find($id);

        if (empty($transaction)) {
            return Response::json(ResponseUtil::makeError('Transaction not found'), 404);
        }

        $transaction = $this->transactionRepository->update($input, $id);

        return $this->sendResponse($transaction->toArray(), 'Transaction updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/transaction/{id}",
     *      summary="Remove the specified Transaction from storage",
     *      tags={"Transaction"},
     *      description="Delete Transaction",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Transaction",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Transaction $transaction */
        $transaction = $this->transactionRepository->find($id);

        if (empty($transaction)) {
            return Response::json(ResponseUtil::makeError('Transaction not found'), 404);
        }

        $transaction->delete();

        return $this->sendResponse($id, 'Transaction deleted successfully');
    }
}

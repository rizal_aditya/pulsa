<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\UserRepository;
use App\Repositories\RoleUserRepository;
use App\Http\Controllers\AppBaseController;
use App\Criteria\UserCriteria;
use Illuminate\Http\Request;
use Illuminate\Routing\UrlGenerator;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;
use Auth;
use file;
use App\User;


class UserController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;
    private $roleUserRepository;
    protected $url;

    public function __construct(UserRepository $userRepo,RoleUserRepository $roleUserRepo,UrlGenerator $url)
    {
        $this->userRepository = $userRepo;
        $this->roleUserRepository = $roleUserRepo;
        $this->url = $url;
    }

     /**
     * Display a listing of the User.
     *
     * @param Request $request
     * @return User
     */
    public function index(Request $request)
    {
            $type = $request['type'];
            $cari = $request->get('cari', null);
            //table Role
           
            $this->userRepository->pushCriteria(new UserCriteria($type));
            $this->userRepository->pushCriteria(new RequestCriteria($request));
            $user = $this->userRepository->paginate(10);


            // search
             if ($cari!=null)
             {
              return redirect('user/?type='.$type.'&search=name:'.$cari.'&searchFields=name:like');
      
             }

        return view('user.index')
            ->with(['user' => $user,'type' => $type]);
    }

      /**
     * Show the form for creating a new User.
     *
     * @return User
     */



    public function login(Request $request)
    {
      
        
        $email = $request['email'];
        $this->userRepository->pushCriteria(new RequestCriteria($request));
        $user   = $this->userRepository->findByField('email',$email);
        

        return view('user.login')
            ->with(['user' => $user,'email'=>$email]);

    }


     public function create(Request $request)
    {

        $type = $request['type'];
        return view('user.create')
            ->with('type', $type);
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return User
     */
    public function store(CreateUserRequest $request )
    {
        $type = $request['type'];
        $input = $request->all();
        
        $input['password'] = bcrypt($request->get('password'));

        $user = $this->userRepository->create($input);

        if($type != "personil")
        {

        $id = $user->id;
        $role_id = '3';
        $dataRole['user_id'] = $id;
        $dataRole['role_id'] = $role_id;
        $roles = $this->roleUserRepository->create($dataRole);

        }


        Flash::success('User saved successfully.');
        return redirect(url('user?type='.$type));
    }

    /**
     * Display the specified User.
     *
     * @param  int $id
     *
     * @return User
     */
    public function show($id,Request $request)
    {
        $user = $this->userRepository->findWithoutFail($id);
        
        $type = $request['type'];
        if (empty($user)) {
            Flash::error('User not found');

            return redirect(url('user?type='.$type));
        }

        return view('user.show')->with(['user' => $user,'type'=> $type]);
        
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param  int $id
     *
     * @return User
     */



     public function edit($id,Request $request)
    {
        $user = $this->userRepository->findWithoutFail($id);



        $type = $request['type'];
        if (empty($user)) {
            Flash::error('User not found');
           return redirect(url('user?type='.$type));

        }

        return view('user.edit')->with(['user'=> $user, 'type' => $type]);
        
        
    }

    /**
     * Update the specified User in storage.
     *
     * @param  int   $id
     * @param UpdateUser $request
     *
     * @return User
     */
    


    public function update($id, UpdateUserRequest $request)
    {
        $user         = $this->userRepository->findWithoutFail($id);
        $data = $request->all();
        $type = $request['type'];
        if (empty($user)) 
        {
            Flash::error('User not found');
            return redirect(url('user?type='.$type));
        }


        $data = $request->except('password', 'password_confirmation');
        if ($request->has('password')) {
            if ($request['password']!='') {
                $data['password'] = bcrypt($request->get('password'));
            }
        }
        
        
        
         if(!empty($request->photo)){  
        
        	
	          if(!empty($user->photo))
		  {
			$file_gambar  =   $this->url->to(''.$user->photo);  
			File::delete($file_gambar);
		  }
	  
	  
	  	
		$file = $request['photo'];
		
      		$original_name = $file->getClientOriginalName();
		$file_path = $file->getPathName();
		$nama 				= gmdate('Ymd').time().'-'.'photo'.'-'.$original_name;
		$dir 				= 'files/';
		$alt				= 'YWlzYWtpQGdtYWlsLmNvbQ==/';
		$photo				= $dir.$alt.$original_name;
		$extensi 			= $this->get_ekstensi($original_name);
		$input['photo'] 			= $photo;
		
		if(move_uploaded_file($file_path, $photo))
		{
		
		
        	$user = $this->userRepository->update($input, $id);
		 
		}
        	  
	        Flash::success('User updated successfully.');
        	return redirect(url('user?type='.$type));

        }else{

        $user = $this->userRepository->update($data, $id);
	 Flash::success('User updated successfully.');
        return redirect(url('user?type='.$type));

        }

       
    }

    /**
     * Remove the specified Person from storage.
     *
     * @param  int $id
     *
     * @return User
     */

    public function enable($id,Request $request)
    {
        $user = $this->userRepository->findWithoutFail($id);
        
        $type = $request['type'];
        if (empty($user)) {
            Flash::error('User not found');

           return redirect(url('user?type='.$type));

        }

        $status = $request['status'];
        
         $secure = sprintf("%04d", mt_rand(1, 9999));
         $data = "LY-";
         $security = $data.$secure;
         $us = User::where('id', $id)->update(['security' => $security,'status'=>$status]);

        Flash::success('User updated successfully.');
        return redirect(url('user?type='.$type));
        
    }


     public function disable($id,Request $request)
    {
        $user = $this->userRepository->findWithoutFail($id);
        
        $type = $request['type'];
        if (empty($user)) {
            Flash::error('User not found');

           return redirect(url('user?type='.$type));

        }

        $status = $request['status'];
       
        $us = User::where('id', $id)->update(['security' => '','status'=>$status]);

        Flash::success('User updated successfully.');
        return redirect(url('user?type='.$type));
        
    }


     public function notification($id,Request $request)
    {
        $user = $this->userRepository->findWithoutFail($id);
        
        $type = $request['type'];
        if (empty($user)) {
            Flash::error('User not found');

           return redirect(url('user?type='.$type));

        }

        $input = $request['status'];
        $user = $this->userRepository->update($request->all(), $id);
        Flash::success('User updated successfully.');
        return redirect(url('user?type='.$type));
        
    }


   

     /**
     * Remove the specified PersonUser from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id,Request $request)
    {
        $type = $request['type'];
        $user = $this->userRepository->findWithoutFail($id);
        $kode = $user->id;

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(url('user?type='.$type));
        }

         $this->userRepository->delete($id);
         
         //$this->personRepository->delete($kode);
         //$this->personUserRepository->delete($kode);
         //$this->personClassRepository->delete($kode);

        Flash::success('User deleted successfully.');

        return redirect(url('user?type='.$type));
    }

public function get_ekstensi($str)
    {
	$i = strrpos($str,".");
		if (!$i) { return ""; }
			$l = strlen($str) - $i;
			$ext = substr($str,$i+1,$l);
	return strtolower($ext);
   }


   
}

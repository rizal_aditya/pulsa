<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateTransactionRequest;
use App\Http\Requests\UpdateTransactionRequest;
use App\Repositories\TransactionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Auth;
use App\Models\Voucher;
use App\Models\Client;
use App\Models\Transaction;
use App\User;


class TransactionController extends AppBaseController
{
    /** @var  TransactionRepository */
    private $transactionRepository;

    public function __construct(TransactionRepository $transactionRepo)
    {
        $this->transactionRepository = $transactionRepo;
    }

    /**
     * Display a listing of the Transaction.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->transactionRepository->pushCriteria(new RequestCriteria($request));
        $transaction = $this->transactionRepository->all();

        return view('transaction.index')
            ->with('transaction', $transaction);
    }

    /**
     * Show the form for creating a new Transaction.
     *
     * @return Response
     */
    public function create()
    {
        return view('transaction.create');
    }

    /**
     * Store a newly created Transaction in storage.
     *
     * @param CreateTransactionRequest $request
     *
     * @return Response
     */
    public function store(CreateTransactionRequest $request)
    {
         $input = $request->all();
         $kode_voucher = $request->code;

         $user = Auth::user();
         $client_id = Auth::user()->client_id;
         $client  = Client::where('id',$client_id)->first();
         $voucher = Voucher::where('code',$kode_voucher)->get();

         $selling = $voucher[0]->denom;
         if($selling == "5000")
         {
             $jual = $voucher[0]->price+75;
         }else if($selling == "15000"){
             $jual = $voucher[0]->price+200;    
         }else if($selling == "10000"){
             $jual = $voucher[0]->price+150;
         }else if($selling == "20000"){
            $jual = $voucher[0]->price+200;    
         }else if($selling == "25000"){
            $jual = $voucher[0]->price+200;
         }else if($selling == "50000"){
            $jual = $voucher[0]->price+300;
         }else if($selling == "100000"){
            $jual = $voucher[0]->price+500;
         }else if($selling == "200000"){  
            $jual = $voucher[0]->price+500;
         }else if($selling == "500000"){ 
            $jual = $voucher[0]->price+1000;
         }

         $input['voucher_id'] = $voucher[0]->id;
         $input['purchase']   = $voucher[0]->price;
         $input['selling']   = $jual;
         $input['client_id']  = $client_id;
         $transaction = $this->transactionRepository->create($input);
         $idtr = $transaction->id;

         $req_user = User::where('id', $request->user_id)->get();
         $code_agen = $req_user[0]->code_agen;
         $code_branch = $req_user[0]->location;
         $code_tr = $client->code_tr;
         $user_id = $client->id_user_era;

        

         
        $param1=date('hms');
        $param2=substr($request->phone,-4); 
        $param3=strrev($param2); 
        $param4 = $code_tr; 
        $hasilXOR=($param1."".$param2) ^ ($param3."".$param4); 
        $signature=base64_encode($hasilXOR);
          
        $url        = 'http://36.37.83.62:1982/cgi-bin/mc/xmlmc_id7.pl';
        $ch         = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, "<elevas>
        <command>TOPUP</command>
        <product>".$request->code."</product>
        <userid>".$user_id."</userid>
        <time>".$param1."</time>
        <msisdn>".$request->phone."</msisdn>
        <trxid>".$idtr."</trxid>
        <kode_store>".$code_agen."</kode_store>
        <kode_branch>".$code_branch."</kode_branch>
        <signature>".$signature."</signature>
        </elevas>" );

        
        $result = curl_exec($ch);
        curl_close($ch);

        $deposit = $result[3];
          
        // print_r($result);
       $tr =  Transaction::where('id', $idtr)->update(['result' => $result]);


        Flash::success('Transaction saved successfully.');

        return redirect(route('transaction.index'));
    }

    /**
     * Display the specified Transaction.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $transaction = $this->transactionRepository->findWithoutFail($id);

        if (empty($transaction)) {
            Flash::error('Transaction not found');

            return redirect(route('transaction.index'));
        }

        return view('transaction.show')->with('transaction', $transaction);
    }

    /**
     * Show the form for editing the specified Transaction.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $transaction = $this->transactionRepository->findWithoutFail($id);

        if (empty($transaction)) {
            Flash::error('Transaction not found');

            return redirect(route('transaction.index'));
        }

        return view('transaction.edit')->with('transaction', $transaction);
    }

    /**
     * Update the specified Transaction in storage.
     *
     * @param  int              $id
     * @param UpdateTransactionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTransactionRequest $request)
    {
        $transaction = $this->transactionRepository->findWithoutFail($id);

        if (empty($transaction)) {
            Flash::error('Transaction not found');

            return redirect(route('transaction.index'));
        }

        $transaction = $this->transactionRepository->update($request->all(), $id);

        Flash::success('Transaction updated successfully.');

        return redirect(route('transaction.index'));
    }

    /**
     * Remove the specified Transaction from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $transaction = $this->transactionRepository->findWithoutFail($id);

        if (empty($transaction)) {
            Flash::error('Transaction not found');

            return redirect(route('transaction.index'));
        }

        $this->transactionRepository->delete($id);

        Flash::success('Transaction deleted successfully.');

        return redirect(route('transaction.index'));
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Http\Controllers\AppBaseController;
use App\Repositories\PostRepository;
use App\Repositories\PostfileRepository;
use App\Repositories\UserRepository;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Http\Request;
use Illuminate\Routing\UrlGenerator;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\PostCriteria;
use App\Criteria\ArsipCriteria;
use Response;
use Auth;
use App\User;
use App\Models\Post;
use App\Models\Device;
use File;


class PostController extends AppBaseController
{
    /** @var  PostRepository */
    private $postRepository;
    private $postfileRepository;
    private $confirmationRepository;
    private $userRepository;
    protected $url;
     

    public function __construct(PostRepository $postRepo, PostfileRepository $postfileRepo,UserRepository $userRepo,UrlGenerator $url)
    {
        $this->postRepository = $postRepo;
        $this->postfileRepository = $postfileRepo;
        $this->userRepository = $userRepo;
        $this->url = $url;
    
        
    }

    /**
     * Display a listing of the Post.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
         $cari = $request->get('cari', null);
         $type = $request['type'];
         $user = Auth::user();

         $this->postRepository->pushCriteria(new PostCriteria($type));
         $this->postRepository->pushCriteria(new RequestCriteria($request));
         // paginator
         $post = $this->postRepository->paginate(10);
         if(!empty($post[0]->id))
         {
         $postfile = $this->postfileRepository->findByField('post_id',$post[0]->id);
         }else{
         $postfile ="";
            
         }  
         // search
         if ($cari!=null)
         {
          return redirect('post/?type='.$type.'&search=title:'.$cari.'&searchFields=title:like');
  
         }

        return view('post.index')
        ->with(['post' => $post,'type' => $type,'postfile'=>$postfile ]);
     
    }

    /**
     * Show the form for creating a new Post.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $type = $request['type'];
       
      

         return view('post.create')
            ->with(['type'=> $type]);
    }

    /**
     * Store a newly created Post in storage.
     *
     * @param CreatePostRequest $request
     *
     * @return Response
     */
    public function store(CreatePostRequest $request)
    {

        $input = $request->all();
        $type = $request['type'];

         $post = $this->postRepository->create($input);        
         Flash::success('Post saved successfully.');
         return redirect(url('post?type='.$type));
    }
    
    
   
    
   	
	
	

   public function arsip($id, Request $request) 
    {
         $this->postRepository->pushCriteria(new ArsipCriteria($id));
         $this->postRepository->pushCriteria(new RequestCriteria($request));
        $post = $this->postRepository->paginate(10);
      

        return view('arsip')
            ->with(['arsip' => $post,'tgl'=>$id]);
    }



    /**
     * Display the specified Post.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id,Request $request)
    {
        $type = $request['type'];
        $user_id = $request['user_id'];
        $branch_id = $request['branch_id'];
        $post = $this->postRepository->findWithoutFail($id);
        $postfile = $this->postfileRepository->findByField('post_id',$id);


        if (empty($post)) {
            Flash::error('Post not found');
            return redirect(url('post?type='.$type));
        }

      
          return view('post.show')->with(['post'=> $post,'postfile'=> $postfile,'type' => $type]);
    }

    /**
     * Show the form for editing the specified Post.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id,Request $request)
    {
        $post = $this->postRepository->findWithoutFail($id);
        $postfile = $this->postfileRepository->findByField('post_id',$id);
        $type = $request['type'];

        $parent = User::where(['type'=>'parent'])->get();
       
        if (empty($post)) {
            Flash::error('Post not found');

            return redirect(url('post?type='.$type));
        }

        return view('post.edit')->with(['post'=> $post, 'postfile'=> $postfile,'type' => $type,'parent'=>$parent]);
    }

    /**
     * Update the specified Post in storage.
     *
     * @param  int              $id
     * @param UpdatePostRequest $request
     *
     * @return Response
     */
     
     
    public function update($id, UpdatePostRequest $request)
    {
        $post = $this->postRepository->findWithoutFail($id);
        $type = $request['type'];
       	$input = $request->all();
      
          
        if(empty($post))
        {
            Flash::error('Post not found');
             return redirect(url('post?type='.$type));
        }
        

		   $post = $this->postRepository->update($input, $id);
	       Flash::success('Post updated successfully.');
	       return redirect(url('post?type='.$type));
      
    }

    /**
     * Remove the specified Post from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id,Request $request)
    {
        $post = $this->postRepository->findWithoutFail($id);
        $type = $request['type'];
  
        if (empty($post)) {
             Flash::error('Post not found');
             return redirect(url('post?type='.$type));
        }
        


        $this->postRepository->delete($id);
        Flash::success('Post deleted successfully.');

            return redirect(url('post?type='.$type));
    }
     

   


    
}

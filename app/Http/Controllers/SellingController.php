<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateSellingRequest;
use App\Http\Requests\UpdateSellingRequest;
use App\Repositories\SellingRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class SellingController extends AppBaseController
{
    /** @var  SellingRepository */
    private $sellingRepository;

    public function __construct(SellingRepository $sellingRepo)
    {
        $this->sellingRepository = $sellingRepo;
    }

    /**
     * Display a listing of the Selling.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->sellingRepository->pushCriteria(new RequestCriteria($request));
        $selling = $this->sellingRepository->all();

        return view('selling.index')
            ->with('selling', $selling);
    }

    /**
     * Show the form for creating a new Selling.
     *
     * @return Response
     */
    public function create()
    {
        return view('selling.create');
    }

    /**
     * Store a newly created Selling in storage.
     *
     * @param CreateSellingRequest $request
     *
     * @return Response
     */
    public function store(CreateSellingRequest $request)
    {
        $input = $request->all();

        $selling = $this->sellingRepository->create($input);

        Flash::success('Selling saved successfully.');

        return redirect(route('selling.index'));
    }

    /**
     * Display the specified Selling.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $selling = $this->sellingRepository->findWithoutFail($id);

        if (empty($selling)) {
            Flash::error('Selling not found');

            return redirect(route('selling.index'));
        }

        return view('selling.show')->with('selling', $selling);
    }

    /**
     * Show the form for editing the specified Selling.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $selling = $this->sellingRepository->findWithoutFail($id);

        if (empty($selling)) {
            Flash::error('Selling not found');

            return redirect(route('selling.index'));
        }

        return view('selling.edit')->with('selling', $selling);
    }

    /**
     * Update the specified Selling in storage.
     *
     * @param  int              $id
     * @param UpdateSellingRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSellingRequest $request)
    {
        $selling = $this->sellingRepository->findWithoutFail($id);

        if (empty($selling)) {
            Flash::error('Selling not found');

            return redirect(route('selling.index'));
        }

        $selling = $this->sellingRepository->update($request->all(), $id);

        Flash::success('Selling updated successfully.');

        return redirect(route('selling.index'));
    }

    /**
     * Remove the specified Selling from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $selling = $this->sellingRepository->findWithoutFail($id);

        if (empty($selling)) {
            Flash::error('Selling not found');

            return redirect(route('selling.index'));
        }

        $this->sellingRepository->delete($id);

        Flash::success('Selling deleted successfully.');

        return redirect(route('selling.index'));
    }
}

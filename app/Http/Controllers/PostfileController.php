<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatePostfileRequest;
use App\Http\Requests\UpdatePostfileRequest;
use App\Repositories\PostfileRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class PostfileController extends AppBaseController
{
    /** @var  PostfileRepository */
    private $postfileRepository;

    public function __construct(PostfileRepository $postfileRepo)
    {
        $this->postfileRepository = $postfileRepo;
    }

    /**
     * Display a listing of the Postfile.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->postfileRepository->pushCriteria(new RequestCriteria($request));
        $postfile = $this->postfileRepository->all();

        return view('postfile.index')
            ->with('postfile', $postfile);
    }

    /**
     * Show the form for creating a new Postfile.
     *
     * @return Response
     */
    public function create()
    {
        return view('postfile.create');
    }

    /**
     * Store a newly created Postfile in storage.
     *
     * @param CreatePostfileRequest $request
     *
     * @return Response
     */
    public function store(CreatePostfileRequest $request)
    {
        $input = $request->all();

        $postfile = $this->postfileRepository->create($input);

        Flash::success('Postfile saved successfully.');

        return redirect(route('postfile.index'));
    }

    /**
     * Display the specified Postfile.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $postfile = $this->postfileRepository->findWithoutFail($id);

        if (empty($postfile)) {
            Flash::error('Postfile not found');

            return redirect(route('postfile.index'));
        }

        return view('postfile.show')->with('postfile', $postfile);
    }

    /**
     * Show the form for editing the specified Postfile.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $postfile = $this->postfileRepository->findWithoutFail($id);

        if (empty($postfile)) {
            Flash::error('Postfile not found');

            return redirect(route('postfile.index'));
        }

        return view('postfile.edit')->with('postfile', $postfile);
    }

    /**
     * Update the specified Postfile in storage.
     *
     * @param  int              $id
     * @param UpdatePostfileRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePostfileRequest $request)
    {
        $postfile = $this->postfileRepository->findWithoutFail($id);

        if (empty($postfile)) {
            Flash::error('Postfile not found');

            return redirect(route('postfile.index'));
        }

        $postfile = $this->postfileRepository->update($request->all(), $id);

        Flash::success('Postfile updated successfully.');

        return redirect(route('postfile.index'));
    }

    /**
     * Remove the specified Postfile from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $postfile = $this->postfileRepository->findWithoutFail($id);

        if (empty($postfile)) {
            Flash::error('Postfile not found');

            return redirect(route('postfile.index'));
        }

        $this->postfileRepository->delete($id);

        Flash::success('Postfile deleted successfully.');

        return redirect(route('postfile.index'));
    }
}

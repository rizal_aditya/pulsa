<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DateTime;

/**
 * @SWG\Definition(
 *      definition="Post",
 *      required={"branch_id", "user_id", "type", "title", "start_date", "end_date", "broadcast", "broadcast_type", "color", "status"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="branch_id",
 *          description="branch_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="type",
 *          description="type",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="title",
 *          description="title",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="content",
 *          description="content",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="broadcast",
 *          description="broadcast",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="broadcast_type",
 *          description="broadcast_type",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="color",
 *          description="color",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Post extends Model
{
    //use SoftDeletes;

    public $table = 'post';
    

    //protected $dates = ['deleted_at'];


    public $fillable = [
       
        'user_id',
        'type',
        'title',
        'content',
        'bg',
        'position_text',
        'position_img',
        'keyword',
        'description',
        'start_date',
        'end_date',
        'broadcast',
        'broadcast_type',
        'parent',
        'color',
        'status',
        'date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [

        'user_id' => 'integer',
        'type' => 'string',
        'title' => 'string',
        'content' => 'string',
        'bg' => 'string',
        'position_text' => 'string',
        'position_img' => 'string',
        'start_date' => 'datetime',
        'end_date' => 'datetime',
        'broadcast' => 'boolean',
        'broadcast_type' => 'string',
        'parent' => 'integer',
        'color' => 'string',
        'status' => 'boolean',
        'date' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
        'user_id' => 'required',
        'type' => 'required',
        'title' => 'required',

        'status' => 'required'
    ];

 
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function postfile()
    {
        return $this->hasMany('App\Models\Postfile');
    }

    /*public function getStartDateAttribute()
    {
        //return date_format(new DateTime($this->attributes['start_date']), 'Y-m-d');  
        //return $this->attributes['start_date']->format('d-m-Y');
    }

    public function getEndDateAttribute()
    {
        return date_format(new DateTime($this->attributes['start_date']), 'Y-m-d');  
    }*/

    /*public function getCreatedAtAttribute()
    {
        return date_format(new DateTime($this->attributes['start_date']), 'd M Y H:i:s');  
    }*/

  
   

    
}

<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DateTime;

/**
 * @SWG\Definition(
 *      definition="Post",
 *      required={"branch_id", "user_id", "type", "title", "start_date", "end_date", "broadcast", "broadcast_type", "color", "status"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="branch_id",
 *          description="branch_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="type",
 *          description="type",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="title",
 *          description="title",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="content",
 *          description="content",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="broadcast",
 *          description="broadcast",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="broadcast_type",
 *          description="broadcast_type",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="color",
 *          description="color",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class User extends Model
{
    //use SoftDeletes;

    public $table = 'users';
    

    //protected $dates = ['deleted_at'];


    public $fillable = [
        'client_id',
        'name',
        'email',
        'password',
        'phone',
        'address',
        'photo',
        'deposite',
        'status',
        'type',
        'code_agen',    
        'owner',
        'location'
       
      
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'client_id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'password' => 'string',
        'phone' => 'string',
        'address' => 'text',
        'photo' => 'string',
        'status' => 'integer',
        'type' => 'string',
        'code_agen' => 'string',
        'owner'=>'string',
        'location' => 'string'
        
        
        
    ];

    protected $rulesets = [

        'creating' => [
            'email'      => 'required|email',
            'password'   => 'required',
           
        ],

        'updating' => [
            'email'      => 'required|email',
            'password'   => '',
            
        ],
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
        'name' => 'required',
        'email' => 'required',
        'password' => 'required', 
        'phone' => 'required',
        'status' => 'required',
        'type' => 'required'
    ];



    
}

<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Menu",
 *      required={"role_id", "name", "url", "icon", "status"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="role_id",
 *          description="role_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="url",
 *          description="url",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="icon",
 *          description="icon",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Menu extends Model
{
   // use SoftDeletes;

    public $table = 'menu';
    

   // protected $dates = ['deleted_at'];


    public $fillable = [
        'role_id',
        'name',
        'url',
        'icon',
        'parent',
        'position',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'role_id' => 'integer',
        'name' => 'string',
        'url' => 'string',
        'icon' => 'string',
        'parent' => 'integer',
        'position' => 'string',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'role_id' => 'required',
        'name' => 'required',
        'url' => 'required',
        'icon' => 'required',
        'status' => 'required'
    ];

  



    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     * @version August 31, 2016, 9:20 am UTC
     **/
    public function role() {
        return $this->belongsTo(
            \App\Role::class,
            'role_id',
            'id'
        );
    }


}

<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Transaction",
 *      required={"user_id", "client_id", "voucher_id", "phone", "status"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="client_id",
 *          description="client_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="voucher_id",
 *          description="voucher_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="phone",
 *          description="phone",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="result",
 *          description="result",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Transaction extends Model
{
    use SoftDeletes;

    public $table = 'transaction';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'client_id',
        'voucher_id',
        'purchase',
        'selling',
        'phone',
        'result',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'client_id' => 'integer',
        'voucher_id' => 'string',
        'purchase' => 'integer',
        'selling' => 'integer',
        'phone' => 'string',
        'result' => 'string',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */


    public static $rules = [
        'code'       => 'required',
        'phone'      => 'required',
        'status'     => 'required'
    ];

 
 
      public function user()
    {
        return $this->belongsTo('App\User');
    }

      public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }

      public function voucher()
    {
        return $this->belongsTo('App\Models\Voucher');
    }
  
}

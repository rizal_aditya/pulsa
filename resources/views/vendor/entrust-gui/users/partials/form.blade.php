<input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="form-group">
    {!! Form::label('client_id', 'Client:') !!}
    {{ Form::select('client_id', App\Models\Client::get()->lists('name', 'id'), $user->client_id, ['class' => 'form-control selectpicker', 'data-live-search'=>'true'])}}
</div>
<div class="form-group">
    <label for="email">Name</label>
    <input type="name" class="form-control" id="name" placeholder="Name" name="name" value="{{ (Session::has('errors')) ? old('name', '') : $user->name }}">
</div>
<div class="form-group">
    <label for="email">Email address</label>
    <input type="email" class="form-control" id="email" placeholder="Email" name="email" value="{{ (Session::has('errors')) ? old('email', '') : $user->email }}">
</div>
<div class="form-group">
    <label for="password">Password</label>
    <input type="password" class="form-control" id="password" placeholder="Password" name="password">
    @if(Route::currentRouteName() == 'entrust-gui::users.edit')
        <div class="alert alert-info">
          <span class="fa fa-info-circle"></span> Leave the password field blank if you wish to keep it the same.
        </div>
    @endif
</div>
@if(Config::get('entrust-gui.confirmable') === true)
<div class="form-group">
    <label for="password">Confirm Password</label>
    <input type="password" class="form-control" id="password_confirmation" placeholder="Confirm Password" name="password_confirmation">
</div>
@endif
<div class="form-group">
    {!! Form::label('birthday', 'Birthday:') !!}
    {!! Form::text('birthday', $user->birthday, ['class' => 'form-control date', 'data-date-format' => 'yyyy-mm-dd']) !!}
</div>
<div class="form-group">
    {!! Form::label('religion', 'Religon:') !!}
    {!! Form::select('religion', ['Islam' => 'Islam', 'Nasrani' => 'Nasari', 'Buddha' => 'Buddha', 'Hindu' => 'Hindu', 'Konghucu' => 'Konghucu', 'Other' => 'Other'], $user->religion, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    {!! Form::tel('phone', $user->phone, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    {!! Form::textarea('address', $user->address, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('photo', 'Photo:') !!}
    {!! Form::text('photo', $user->photo, ['class' => 'form-control', 'name'=>'photo', 'readonly'=>'readonly', 'placeholder'=>'Gunakan button "Select Image"']) !!}
    <a href="{!! route('fileupload') !!}" class="btn btn-success popup_selector form-control" data-inputid="photo">Select Image</a>
</div>
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}<br/>
    {{ Form::radio('status', 1, isset($user) ? $user->status == 1 : false) }} Yes<br>
    {{ Form::radio('status', 0, isset($user) ? $user->status == 0 : true) }} No
</div>
<div class="form-group">
    <label for="roles">Roles</label>
    <br/>
    @foreach($roles as $index => $role)
        <input type="checkbox" name="roles[]" value="{{ $index }}" {{ ((in_array($index, old('roles', []))) || ( ! Session::has('errors') && $user->roles->contains('id', $index))) ? 'checked' : '' }}>{{ $role }}<br>
    @endforeach
</div>

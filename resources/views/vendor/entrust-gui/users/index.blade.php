@extends(Config::get('entrust-gui.layout'))

@section('content')












    <section class="content-header">
        <h1 class="pull-left">User</h1>
        <h1 class="pull-right">
           <a class="btn btn-labeled btn-primary" href="{{ route('entrust-gui::users.create') }}"><span class="btn-label"><i class="fa fa-plus"></i></span>{{ trans('entrust-gui::button.create-user') }}</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        <div class="clearfix"></div>
        <div class="box box-primary">
          
            <div class="clearfix"></div>

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                <table class="table table-striped">
                  <tr>
                    <th>Photo</th>
                    <th>Name</th>
                    <th>CLient</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th>Actions</th>
                  </tr>
                  @foreach($users as $user)
                    <tr>
                      <td> <img src="{!! Auth::user()->photo == '' ? config('app.photo_default') : url(Auth::user()->photo)  !!}" class="img-circle" width="50" height="50" alt="User Image"/></td>
                       <td>{{ $user->client->name }}</td>
                      <td>{{ $user->name }}</td>
                      <td>{{ $user->email }}</td>
                      <td>
                         @if($user->status>0)
                            <a href="{!! url('user/'.$user->id.'/notification?status=0') !!}">Aktif</a>
                         @else
                            <a href="{!! url('user/'.$user->id.'/notification?status=1') !!}">Non Aktif</a>
                         @endif
                      </td>
                      <td class="col-xs-3">
                        <form action="{{ route('entrust-gui::users.destroy', $user->id) }}" method="post">
                          <input type="hidden" name="_method" value="DELETE">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <a class="btn btn-labeled btn-default" href="{{ route('entrust-gui::users.edit', $user->id) }}"><span class="btn-label"><i class="fa fa-pencil"></i></span>{{ trans('entrust-gui::button.edit') }}</a>
                          <button type="submit" class="btn btn-labeled btn-danger"><span class="btn-label"><i class="fa fa-trash"></i></span>{{ trans('entrust-gui::button.delete') }}</button>
                        </form>
                      </td>
                    </tr>
                  @endforeach
                </table>
                <div class="text-center">
                  {!! $users->render() !!}
                </div>
            </div>
        </div>     
    </div>
@endsection

 <?php 

header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Mitra_all_ais_auto_care.xls");

?>

<table border="1">
    <tr>
    		<th>No.</th>
		<th>Register ID:</th>
		<th>Name Company</th>
		<th>Email</th>
		<th>Phone Office</th>
		<th>HP:</th>
		<th>Whatsapp:</th>
		<th>BBM:</th>
		<th>Instagram:</th>
		<th>Address:</th>
		<th>Kecamatan:</th>
		<th>Kabupaten:</th>
		<th>Owner</th>
		<th>Manager</th>
		<th>Phone Manager:</th>
		<th>Bussines Primary:</th>
		<th>Bussines Secondary:</th>
		<th>Etc:</th>
		<th>Qty SDM:</th>
		<th>Menurut Anda, apa keunggulan usaha Anda dibandingkan usaha sejenis?</th>
		<th>Apakah Anda menyediakan layanan kunjungan/pengantaran/jemputan?</th>
		<th>Apakah Anda bersedia jika Ais Auto Care membantu meningkatkan jumlah pelanggannya?</th>
		<th>Apakah usaha Anda ini  memiliki daftar harga layanan standar?</th>
		<th colspan='2'>Berapa kisaran biaya layanan usaha Anda (dalam rupiah)?</th>
		<th >Apa harapan anda dengan menjadi mitra Ais Auto Care?</th>
		<th >Latitude</th>
		<th >Longitude</th>
		<th>Create At </th>
	</tr>
	
	<?php $i=1; if(!empty($client)){ foreach($client as $row){ ?>
		<tr>
			<td><?=$i;?></td>
			<td>{!! $row->id !!}</td>
			<td>{!! $row->name !!}</td>
			<td>{!! $row->email !!}</td>
			<td>@if($row->phone =="") @else {!! $row->phone !!} @endif</td>
			<td>@if($row->hp =="") @else {!! $row->hp !!} @endif</td>
			<td>@if($row->whatapps =="") @else {!! $row->whatapps !!} @endif</td>
			<td>{!! $row->bbm !!}</td>
			<td>{!! $row->instagram !!}</td>
			<td>{!! $row->address !!}</td>
			<td>{!! $row->kecamatan !!}</td>
			<td>{!! $row->kabupaten !!}</td>
			<td>{!! $row->owner !!}</td>
			<td>{!! $row->manager!!}</td>
			<td>{!! $row->phone_manager !!}</td>
			<td>{!! $row->bussines_primary !!}</td>
			<td>{!! $row->bussines_secondary !!}</td>
			<td>{!! $row->etc !!}</td>
			<td>{!! $row->sdm !!}</td>
			<td>{!! $row->essay1 !!}</td>
			
			<td>@if($row->pilihan1 =='1')YA @else TIDAK @endif</td>
			<td>@if($row->pilihan2 =='1')YA @else TIDAK @endif</td>
			<td>@if($row->pilihan3 =='1')YA @else TIDAK @endif</td>
			<td>Termahal : {!! $row->termahal !!}  </td>
			<td>Termurah : {!! $row->termurah !!}</td>
			<td>{!! $row->essay3 !!}</td>
			<td>{!! $row->latitude !!}</td>
			<td>{!! $row->longitude !!}</td>
			<td>{!! $row->created_at->format('d F Y') !!}</td>
		</tr>
<?php $i++; } } ?>
	
</table>
 
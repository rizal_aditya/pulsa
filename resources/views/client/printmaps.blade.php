<html> 
<head> 
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" /> 
  <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/dataTables.bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('/font-awesome/4.5.0/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/font-awesome.css') }}" media="screen">
    <link rel="stylesheet" href="{{ asset('/ionicons/2.0.1/css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/dist/css/skins/_all-skins.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/plugins/datepicker/datepicker3.css') }}">
    <link rel="stylesheet" href="{{ asset('/plugins/timepicker/bootstrap-timepicker.css') }}">
    <link href="{{ asset('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/css/colorbox.css') }}" rel="stylesheet">
<link href="{{ asset('/css/ui.tabs.css') }}" rel="stylesheet"> 
<script src="{{ asset('/js/jQuery-2.2.0.min.js') }}"></script>
<script type="text/javascript">
window.print()
</script>     

  <title>Print Maps</title>
  
<style type="text/css">
#container{
    margin: 0px auto;
    width: 900px;
    height: auto;
    
}
#container .left {
    float: left;
    width: 500px;
}
#container .left .peta{
    float: left;
    width:100%;
    height: 383px;
    position: relative;
    background-color: #E5E3DF;
    overflow: hidden; 
    margin: 16px 0px; 
}
#container .right {
    float: right;
    width: 400px;
   
}
#container .right table{
 font-size: 12px;
}

#container .ft{
float: left;
width: 100%;
background: #c5c2c2;
color: #545050;
padding: 0px 5px;
border: 1px solid #c6c6c6;
border-radius: 3px;

}
</style>
</head> 
<body>
 

  


  <div id="container">
    <div class="left">
        @include('client.js')
    </div>

    <div class="right">
      <section class="content">
          <div class="row">
                <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Keterangan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              
              <table class="table table-bordered">
                <tbody><tr>
                  <th style="width: 10px">Icon</th>
                  <th>Kategori</th>
                  
                  <th style="width: 40px">Total</th>
                </tr>

                @if($status !=null && $kategori !=null)

                @if($status !='2')
                   
                         @if($kategori !="0")

                                 @foreach(App\Models\Category::where(['id'=>$kategori])->OrderBy('id','decs')->get() as $row)
                                <tr>
                                  <td><img src="{!! $row->photo !!}"/></td>
                                  <td>{!! $row->name !!}</td>
                                  <td><span class="badge bg-red">( {!! $category = DB::table('client')->where(['status'=>$status,'category_id'=> $row->id] )->count(); !!} ) </span></td>
                                </tr>
                               @endforeach



                         @else

                               @foreach(App\Models\Category::get() as $row)
                                <tr>
                                  <td><img src="{!! $row->photo !!}"/></td>
                                  <td>{!! $row->name !!}</td>
                                  <td><span class="badge bg-red">( {!! $category = DB::table('client')->where(['status'=>$status,'category_id'=> $row->id] )->count(); !!} ) </span></td>
                                </tr>
                               @endforeach


                        @endif


               @else  


                          @if($kategori !='0')

                             @foreach(App\Models\Category::where(['id'=>$kategori])->OrderBy('id','decs')->get() as $row)
                                <tr>
                                  <td><img src="{!! $row->photo !!}"/></td>
                                  <td>{!! $row->name !!}</td>
                                  <td><span class="badge bg-red">( {!! $category = DB::table('client')->where(['category_id'=> $row->id] )->count(); !!} ) </span></td>
                                </tr>
                               @endforeach

                          @else

                             @foreach(App\Models\Category::get() as $row)
                                <tr>
                                  <td><img src="{!! $row->photo !!}"/></td>
                                  <td>{!! $row->name !!}</td>
                                  <td><span class="badge bg-red">( {!! $category = DB::table('client')->where(['category_id'=> $row->id] )->count(); !!} ) </span></td>
                                </tr>
                               @endforeach

                        @endif       
               @endif
          @else

                              @foreach(App\Models\Category::get() as $row)
                                <tr>
                                  <td><img src="{!! $row->photo !!}"/></td>
                                  <td>{!! $row->name !!}</td>
                                  <td><span class="badge bg-red">( {!! $category = DB::table('client')->where(['category_id'=> $row->id] )->count(); !!} ) </span></td>
                                </tr>
                               @endforeach

          @endif
               
              </tbody></table>
            </div>
            <!-- /.box-body -->
           
          </div>
          

            </div>
           </div>
        </section>   
    
    </div>
</div>



<div id="container">
  <div class="ft"><h4><b>Nama Detail Mitra</b></h4></div>
  <ul style="float:left;width:100%;padding:0px;">


         @if($status !=null && $kategori !=null)

                @if($status !='2')
                   
                         @if($kategori !="0")

                                 @foreach(App\Models\Client::where(['category_id'=>$kategori,'status'=>$status])->OrderBy('id','decs')->get() as $client)
                                 <li style="float:left;padding:5px;list-style: none;"><img width="20" height="20" src="{!!  $client->category->photo  !!}" /> {!!  $client->name  !!}</li>
                                 @endforeach



                         @else

                               @foreach(App\Models\Client::where(['status'=>$status])->OrderBy('id','decs')->get() as $client)
                                 <li style="float:left;padding:5px;list-style: none;"><img width="20" height="20" src="{!!  $client->category->photo  !!}" /> {!!  $client->name  !!}</li>
                               @endforeach




                        @endif


               @else  


                          @if($kategori !='0')

                             @foreach(App\Models\Client::where(['category_id'=>$kategori])->OrderBy('id','decs')->get() as $client)
                                 <li style="float:left;padding:5px;list-style: none;"><img width="20" height="20" src="{!!  $client->category->photo  !!}" /> {!!  $client->name  !!}</li>
                               @endforeach

                          @else

                             @foreach(App\Models\Client::where('type','mitra')->OrderBy('id','decs')->get() as $client)
                                 <li style="float:left;padding:5px;list-style: none;"><img width="20" height="20" src="{!!  $client->category->photo  !!}" /> {!!  $client->name  !!}</li>
                               @endforeach

                        @endif       
               @endif

          @else     
                              @foreach(App\Models\Client::where('type','mitra')->OrderBy('id','decs')->get() as $client)
                                 <li style="float:left;padding:5px;list-style: none;"><img width="20" height="20" src="{!!  $client->category->photo  !!}" /> {!!  $client->name  !!}</li>
                               @endforeach
              
          @endif 
          </ul>
</div>
<script src="{{ asset('/js/bootstrap.min.js') }}"></script>
</body>
</html>
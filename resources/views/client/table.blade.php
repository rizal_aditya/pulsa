<table class="table table-responsive" id="client-table">
    <thead>
        <th>Name</th>
        <th>Role Id</th>
        <th>Phone</th>
        <th>Email</th>
        <th>Address</th>
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($client as $client)
        <tr>
            <td>{!! $client->name !!}</td>
            <td>{!! $client->role_id !!}</td>
            <td>{!! $client->phone !!}</td>
            <td>{!! $client->email !!}</td>
            <td>{!! $client->address !!}</td>
            <td>{!! $client->status !!}</td>
            <td>
                {!! Form::open(['route' => ['client.destroy', $client->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('client.show', [$client->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('client.edit', [$client->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
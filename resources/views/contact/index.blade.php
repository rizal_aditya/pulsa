   @include('frontend.header')
        @include('frontend.page')
        <main role="main">
                 <div id="intro-wrap" class="">
                    <div id="intro" class="preload">
                        @foreach($contact as $contact)                    
                        <div class="map" data-maplat="{{ $contact->latitude }}" data-maplon="{{ $contact->longitude }}" data-mapzoom="6" data-color="aqua" data-height="300" data-img="{{ asset('/img/marker.png') }}" data-info="{{ $contact->address }} </br> Email to : {{ $contact->email }}</br>Phone : {{ $contact->phone }}"></div>                            
                        @endforeach
                    </div><!-- intro -->
                 </div><!-- intro-wrap -->
            <div id="main">
                           
            </div><!-- id-main -->
        </main><!-- main -->
 @include('frontend.footer')

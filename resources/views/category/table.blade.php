<table class="table table-responsive" id="category-table">
    <thead>
        <th>Photo</th>
        <th>Name</th>
        
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($category as $category)
        <tr>
            <td><img width="40" height="40" src="{!! $category->photo !!}"/></td>
            <td>{!! $category->name !!}</td>
            
            <td>  @if($category->status>0)
               Aktif
             @else
                Non Aktif
             @endif</td>
            <td>
                {!! Form::open(['route' => ['category.destroy', $category->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('category.show', [$category->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('category.edit', [$category->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
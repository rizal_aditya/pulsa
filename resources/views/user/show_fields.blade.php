

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $user->name !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $user->email !!}</p>
</div>

@if($type == 'parent') 

@elseif($type == 'client')
@role('user')
<!-- Departement Field -->
<div class="form-group">
    {!! Form::label('departement', 'Departement:') !!}
    <p>{!! $user->departement !!}</p>
</div>

<!-- Nip Field -->
<div class="form-group">
    {!! Form::label('nip', 'Nip:') !!}
    <p>{!! $user->nip !!}</p>
</div>
@endrole
@endif



<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{!! $user->phone !!}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    <p>{!! $user->address !!}</p>
</div>


@if($type == 'parent')
@elseif($type != 'client')
<!-- Education Field -->
<div class="form-group">
    {!! Form::label('education', 'Education:') !!}
    <p>{!! $user->education !!}</p>
</div>
@endif

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>
         @if($user->status>0)
            No
            @else
            Yes
          @endif  

    </p>
</div>





<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $user->created_at->format('d F Y') !!}</p>
</div>





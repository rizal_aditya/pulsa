<div class="table-responsive">
<table class="table table-striped table-bordered table-hover" id="dynamic-table" >

    <thead>
        <th>Photo</th>
        <th>Name</th>
        @if($type == "personil")
        <th>Struktur</th>
        <th>Position</th>
        <th>Description</th>
        @elseif($type == "mitra")
        <th>Position</th>
         <th>Description</th>
        @else
        <th>Email</th>
        <th>Address</th>
        @endif
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
       
     @foreach($user as $user)
        
        <tr>
            <td><img width="50" height="50" src="{!! $user->photo == '' ? config('app.photo_default') : url($user->photo)  !!} "></td>
            
             <td>{!! $user->name !!}</td>
             @if($type == "personil")
             <td>{!! $user->owner !!}</td>
             <td>{!! $user->position !!}</td>
             <td>{!! $user->description !!}</td>
             @elseif($type == "mitra")
             <td>{!! $user->position !!}</td>
             <td>{!! $user->description !!}</td>
             @else
             <td>{!! $user->email !!}</td>
             <td>{!! $user->address !!}</td>
             @endif
             <td> 
                @if($user->status>0)
                <a href="{!! url('user/'.$user->id.'/disable?status=0'.'&type='.$type) !!}">Aktif</a>
             @else
                <a href="{!! url('user/'.$user->id.'/enable?status=1'.'&type='.$type) !!}">Non Aktif</a>
             @endif

             </td>
             @role('user')
             <td>
              
                {!! Form::open(['route' => ['user.destroy', $user->id.''.'?type='.$type], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! url('user/'.$user->id.'/?type='.$type) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! url('user/'.$user->id.'/edit?type='.$type) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                  	
                  	
                   
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    
                </div>
                {!! Form::close() !!}
             </td>
             @endrole

              @role('admin')
             <td>
               {!! Form::open(['route' => ['user.destroy', $user->id.''.'?type='.$type], 'method' => 'delete']) !!}
                <div class='btn-group'>

                    <a href="{!! route('user.show', [$user->id.'?type='.$type]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    @if($type <> 'parent')
                     <a href="{!! url('user/'.$user->id.'/edit?type='.$type) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    @endif
                     @if($type == "personil")
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    @endif
                </div>
                {!! Form::close() !!}
             </td>
             @endrole


        </tr>
    @endforeach
    </tbody>
</table>
</div>




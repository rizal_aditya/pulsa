<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{!! config('app.title') !!}</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- Bootstrap 3.3.6 -->
   <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">
   <link rel="stylesheet" href="{{ asset('/font-awesome/4.5.0/css/font-awesome.min.css') }}">
   <link rel="stylesheet" href="{{ asset('/ionicons/2.0.1/css/ionicons.min.css') }}">
   <link rel="stylesheet" href="{{ asset('/dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/bootstrap-select.min.css') }}">
   <link rel="stylesheet" href="{{ asset('/dist/css/skins/_all-skins.min.css') }}">
   <link rel="stylesheet"  href="{{ asset('/css/blue.css') }}">
  <script src="{{ asset('/js/jQuery-2.2.0.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/jquery.autocomplete.min.js') }}"></script>

  

</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="{{ url('/home') }}">{!! config('app.title') !!}</a>
    </div>

    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>

        <div id="searchfield">
        <form id="searchform" method="post" action="{{ url('/access/check') }}">
            {!! csrf_field() !!}

            <div class="input-group">
               @if ($errors->has('email'))
                    <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
              <input name="email" placeholder="Email ..." class="form-control" type="email">
              <span class="input-group-btn">
                <button type="submit" class="btn btn-primary btn-flat">Sign In</button>
              </span>
            </div>




        </form>
      </br>


         <a href="{{ url('/password/reset') }}">I forgot my password</a><br>
        <a href="{{ url('/register') }}" class="text-center">Register a new membership</a>


 

   


  <style type="text/css">


/* SEARCHRESULTS */
#searchresults {
    border: 1px solid #d5d2d2;
    background-color: #f9f9f9;
    font-size: 10px;
    line-height: 14px;
    z-index: 1000;
    position: absolute;
    border-radius: 3px;
    margin: 50px 0px 0px 0px;
    width: 320px;
}

#searchresults .positon{
  margin: 0px auto;
  width: 300px;


}

#searchresults .positon ul{
    float: left;
    width: 100%;
    margin: none;
    padding: 0px;
    list-style: none;
}

#searchresults .positon ul li{
float: left;
margin: 5px 0px 5px 0px;
width: 100%;
cursor: pointer;
border-bottom: 1px solid #ddd;
  }

  #searchresults .positon ul li img{
    float: left;
    padding: 0px 0px 3px 0px;

  }

#searchresults .positon ul li .searchheading{
float: left;
width: auto;
font-size: 12px;
font-weight: bold;
padding: 0px 6px;
width: 250px;


}
#searchresults .positon ul li .emaile{
float: left;
width: auto;
font-size: 10px;
padding: 0px 6px;
width: 250px;
}

.autocomplete-suggestions { border: 1px solid #999; background: #fff; cursor: default; overflow: auto; }
.autocomplete-suggestion { padding: 10px 5px; font-size: 1.2em; white-space: nowrap; overflow: hidden; }
.autocomplete-selected { background: #f0f0f0; }
.autocomplete-suggestions strong { font-weight: normal; color: #3399ff; }
  </style>

  </div><!-- @end #searchfield -->


       

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->



<script  src="{{ asset('/js/jquery.autocomplete.min.js') }}"></script>
<script src="{{ asset('/js/bootstrap.min.js') }}"></script>
<script  src="{{ asset('/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('/js/icheck.min.js') }}"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
<script>
$(function(){
  var currencies = 
  [

    @foreach (App\User::join('role_user', 'users.id', '=', 'role_user.user_id')->get() as $user)
    
    { 
      value: '{{ $user->email }} ', 
      data: '{{ $user->client_id }}', 
    
      name:'{{ $user->name }}', 
    },  
        
    @endforeach

  ];

  
  // setup autocomplete function pulling from currencies[] array
  $('#autocomplete').autocomplete(
      {
        lookup: currencies,
        onSelect: function (suggestion) 
        {
           var msg = '<input type="hidden" name="client_id" value="' + suggestion.data + '"> <input type="hidden" name="name" value="' + suggestion.name + '" disabled>';
           $('#outputcontent').html(msg);
           // var email = ''+ suggestion.value +'';

           // $.ajax({
           //    type :"GET",
           //    url :"{{ url('user/login') }}",
           //    data : email,
           //    success : function()
           //    {
                
           //      $("#autocomplete").html(msg);

           //    },
           //    error : function()
           //    {
           //      alert("Gagal");
           //    }
           //  });
        }
      }
  );
  

});
</script>
</body>
</html>

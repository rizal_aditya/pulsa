<table class="table table-responsive" id="survey-table">
    <thead>
        <th>Name</th>
        <th>Description</th>
        <th>Type</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($survey as $survey)
        <tr>
            <td>{!! $survey->name !!}</td>
            <td>{!! $survey->description !!}</td>
            <td>@if($survey->status == '1')Pilihan @else Essay @endif</td>
            <td>
                {!! Form::open(['route' => ['survey.destroy', $survey->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('survey.show', [$survey->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('survey.edit', [$survey->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
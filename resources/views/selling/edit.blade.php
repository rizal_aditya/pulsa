@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Selling
        </h1>
    </section>
    <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($selling, ['route' => ['selling.update', $selling->id], 'method' => 'patch']) !!}

                        @include('selling.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
    </div>
@endsection
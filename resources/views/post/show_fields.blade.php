@role('admin')

<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $post->id !!}</p>
</div>


<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User:') !!}
    <p>{!! $post->user->name !!}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{!! $post->type !!}</p>
</div>
@endrole

@if($type =='massage')
<!-- Title Field -->
<div class="form-group">
    {!! Form::label('keyword', 'Name:') !!}
    <p>{!! $post->keyword !!}</p>
</div>

<div class="form-group">
    {!! Form::label('position_img', 'Company:') !!}
    <p>{!! $post->position_img !!}</p>
</div>

<div class="form-group">
    {!! Form::label('bg', 'Email:') !!}
    <p>{!! $post->bg !!}</p>
</div>

<div class="form-group">
    {!! Form::label('position_text', 'Phone:') !!}
    <p>{!! $post->position_text !!}</p>
</div>


@endif



@if($type =='personil')

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('id', 'ID:') !!}
    <p>{!! $post->id !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Name:') !!}
    <p>{!! $post->title !!}</p>
</div>



<!-- Content Field -->
<div class="form-group">
    {!! Form::label('content', 'Description:') !!}
    <p>{!! $post->content !!}</p>
</div>


<!-- Title Field -->
<div class="form-group">
    {!! Form::label('keyword', 'Employment:') !!}
    <p>{!! $post->keyword !!}</p>
</div>


<!-- Title Field -->
<div class="form-group">
    {!! Form::label('position_text', 'Position:') !!}
    <p>{!! $post->position_text !!}</p>
</div>


<!-- Title Field -->
<div class="form-group">
    {!! Form::label('description', 'Facebook:') !!}
    <p>{!! $post->description !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('position_img', 'Twitter:') !!}
    <p>{!! $post->position_img !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('broadcast_type', 'Google:') !!}
    <p>{!! $post->broadcast_type !!}</p>
</div>
<!-- Title Field -->
<div class="form-group">
{!! Form::label('bg', 'Image:') !!}
<p> <img width="100" height="100" src="{!! empty($post->bg) ? config('app.photo_default') : url($post->bg)  !!}" /></p>
</div>

@else
<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{!! $post->title !!}</p>
</div>

<!-- Content Field -->
<div class="form-group">
    {!! Form::label('content', 'Content:') !!}
    <p>{!! $post->content !!}</p>
</div>



@endif


@if($type =="calendar" && $type=="invitation")
<!-- Start Date Field -->
<div class="form-group">
    {!! Form::label('start_date', 'Start Date:') !!}
    <p>{!! $post->start_date !!}</p>
</div>

<!-- End Date Field -->
<div class="form-group">
    {!! Form::label('end_date', 'End Date:') !!}
    <p>{!! $post->end_date !!}</p>
</div>


@endif



@if($type =="calendar")
<!-- Color Field -->
<div class="form-group">
    {!! Form::label('color', 'Color:') !!}
    <p>{!! $post->color !!}</p>
</div>

@endif


@role('admin')

@endrole
@foreach($postfile as $i => $postfile)
<!-- Color Field -->
<div class="form-group">
    {!! Form::label('file', 'File:') !!}
    <p><img src="{!! url($postfile->file) !!}" width="100" height="100" ></p>
</div>
@endforeach
<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>@if($post->status == '1')yes @else No @endif</p>
</div>




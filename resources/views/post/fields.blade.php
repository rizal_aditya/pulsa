<!-- User Id Field -->
@role('user') 
<div class="form-group col-sm-12">
    {!! Form::hidden('user_id', Auth::user()->id, ['class' => 'form-control']) !!}
    {!! Form::hidden('type', $type, ['class' => 'form-control']) !!}
        
</div>
@endrole

@if($type == 'testimonials')
<div class="form-group col-sm-12">
    {!! Form::label('title', 'Name:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('position_text', 'Employment:') !!}
    {!! Form::text('position_text', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('content', 'Message:') !!}
    {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
</div>
{!! Form::hidden('type', $type , ['class' => 'form-control']) !!}

<div class="form-group col-sm-12">
@if(!empty($post->bg))
	<img src="{!! url($post->bg) !!}" width="100" height="100">
@endif	
</div>

<div class="form-group col-sm-12">
 {!! Form::label('bg', 'Photo:') !!}   
 {!! Form::text('bg', null, ['class' => 'form-control', 'name'=>'bg', 'readonly'=>'readonly', 'placeholder'=>'Gunakan button "Select Image size"']) !!}
    <a href="{!! route('fileupload') !!}" class="btn btn-success popup_selector form-control" data-inputid="bg">Select Image</a>
</div>
</div>


<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('status', 'Status:') !!}<br/>
    {{ Form::radio('status', 1, isset($post) ? $post->status == 1 : false) }} Yes<br>
    {{ Form::radio('status', 0, isset($post) ? $post->status == 0 : true) }} No
</div>
@elseif($type == "berita")
<div class="form-group col-sm-12">
    {!! Form::label('title', 'Name:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('content', 'Content:') !!}
    {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
</div>

{!! Form::hidden('type', $type , ['class' => 'form-control']) !!}


<div class="form-group col-sm-12">
@if(!empty($post->bg))
	<img src="{!! url($post->bg) !!}" width="100" height="100">
@endif	
</div>


<div class="form-group col-sm-12">
 {!! Form::label('bg', 'Image:') !!}   
 {!! Form::text('bg', null, ['class' => 'form-control', 'name'=>'bg', 'readonly'=>'readonly', 'placeholder'=>'Gunakan button "Select Image size min 1169x487"']) !!}
    <a href="{!! route('fileupload') !!}" class="btn btn-success popup_selector form-control" data-inputid="bg">Select Image</a>

</div>

<!-- Position_text Field -->
<div class="form-group col-sm-12">
    {!! Form::label('position_text', 'Position:') !!}<br/>
    {{ Form::radio('position_text', 'active' , isset($post) ? $post->position_text == 'active' : true) }} Primary<br>
    {{ Form::radio('position_text', '', isset($post) ? $post->position_text == '' : false) }} Secondary
</div>

<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('status', 'Status:') !!}<br/>
    {{ Form::radio('status', 1, isset($post) ? $post->status == 1 : false) }} Yes<br>
    {{ Form::radio('status', 0, isset($post) ? $post->status == 0 : true) }} No
</div>


@elseif($type == "teknis")
<div class="form-group col-sm-12">
    {!! Form::label('title', 'Name:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

{!! Form::hidden('type', $type , ['class' => 'form-control']) !!}


<div class="form-group col-sm-12">
@if(!empty($post->bg))
	<img src="{!! empty($post->bg) ? config('app.photo_default') : url($post->bg)  !!}" width="100" height="100">
@endif	
</div>




<div class="form-group col-sm-12">
 {!! Form::label('bg', 'Image:') !!}   
 {!! Form::text('bg', null, ['class' => 'form-control', 'name'=>'bg', 'readonly'=>'readonly', 'placeholder'=>'Gunakan button "Select Image size min 145x137"']) !!}
    <a href="{!! route('fileupload') !!}" class="btn btn-success popup_selector form-control" data-inputid="bg">Select Image</a>
    
</div>



<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('status', 'Status:') !!}<br/>
    {{ Form::radio('status', 1, isset($post) ? $post->status == 1 : false) }} Yes<br>
    {{ Form::radio('status', 0, isset($post) ? $post->status == 0 : true) }} No
</div>



@elseif($type == "personil")
<div class="form-group col-sm-12">
    {!! Form::label('title', 'Name:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Position_text Field -->
<div class="form-group col-sm-12">
    {!! Form::label('keyword', 'Employment:') !!}<br/>
    {!! Form::text('keyword', null , ['class' => 'form-control']) !!}

</div>


<!-- Position_text Field -->
<div class="form-group col-sm-12">
    {!! Form::label('position_text', 'Position:') !!}<br/>
    {!! Form::select('position_text', ['kiri_atas'=>'Kiri Atas','kanan_atas'=>'Kanan Atas','bawah'=>'Bawah'] ,null , ['class' => 'form-control selectpicker']) !!}

</div>



<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('content', 'Deskripsi:') !!}
    {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
</div>





<!-- Position_text Field -->
<div class="form-group col-sm-12">
 
    {!! Form::hidden('position_img', null , ['class' => 'form-control']) !!}

</div>







{!! Form::hidden('type', $type , ['class' => 'form-control']) !!}


<div class="form-group col-sm-12">
@if(!empty($post->bg))
	<img src="{!! empty($post->bg) ? config('app.photo_default') : url($post->bg)  !!}" width="100" height="100">
@endif	
</div>




<div class="form-group col-sm-12">
 {!! Form::label('bg', 'Image:') !!}   
 {!! Form::text('bg', null, ['class' => 'form-control', 'name'=>'bg', 'readonly'=>'readonly', 'placeholder'=>'Gunakan button "Select Image size min 145x137"']) !!}
    <a href="{!! route('fileupload') !!}" class="btn btn-success popup_selector form-control" data-inputid="bg">Select Image</a>    
</div>







<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('status', 'Status:') !!}<br/>
    {{ Form::radio('status', 1, isset($post) ? $post->status == 1 : false) }} Yes<br>
    {{ Form::radio('status', 0, isset($post) ? $post->status == 0 : true) }} No
</div>


















@elseif($type == "slider")

<div class="form-group col-sm-12">
    {!! Form::label('title', 'Name:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>


{!! Form::hidden('type', $type , ['class' => 'form-control']) !!}


<div class="form-group col-sm-12">
@if(!empty($post->bg))
	<img src="{!! url($post->bg) !!}" width="100" height="100">
@endif	
</div>




<div class="form-group col-sm-12">
 {!! Form::label('bg', 'Image:') !!}   
 {!! Form::text('bg', null, ['class' => 'form-control', 'name'=>'bg', 'readonly'=>'readonly', 'placeholder'=>'Gunakan button "Select Image size min 1169x487"']) !!}
    <a href="{!! route('fileupload') !!}" class="btn btn-success popup_selector form-control" data-inputid="bg">Select Image</a>
        
</div>

<!-- Position_text Field -->
<div class="form-group col-sm-12">
    {!! Form::label('position_text', 'Position:') !!}<br/>
    {{ Form::radio('position_text', 'active' , isset($post) ? $post->position_text == 'active' : true) }} Primary<br>
    {{ Form::radio('position_text', '', isset($post) ? $post->position_text == '' : false) }} Secondary
</div>

<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('status', 'Status:') !!}<br/>
    {{ Form::radio('status', 1, isset($post) ? $post->status == 1 : false) }} Yes<br>
    {{ Form::radio('status', 0, isset($post) ? $post->status == 0 : true) }} No
</div>

@elseif($type == "manfaat")
<div class="form-group col-sm-12">
    {!! Form::label('title', 'Name:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('content', 'Content:') !!}
    {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-12">
@if(!empty($post->bg))
	<img src="{!! url($post->bg) !!}" width="100" height="100">
@endif	
</div>

{!! Form::hidden('type', $type , ['class' => 'form-control']) !!}

<div class="form-group col-sm-12">
 {!! Form::label('bg', 'Image:') !!}   
{!! Form::text('bg', null, ['class' => 'form-control', 'name'=>'bg', 'readonly'=>'readonly', 'placeholder'=>'Gunakan button "Select Image size min 81x81"']) !!}
    <a href="{!! route('fileupload') !!}" class="btn btn-success popup_selector form-control" data-inputid="bg">Select Image</a>
</div>

<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('status', 'Status:') !!}<br/>
    {{ Form::radio('status', 1, isset($post) ? $post->status == 1 : false) }} Yes<br>
    {{ Form::radio('status', 0, isset($post) ? $post->status == 0 : true) }} No
</div>


@elseif($type == "layanan")

<div class="form-group col-sm-12">
    {!! Form::label('title', 'Name:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('content', 'Content:') !!}
    {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-12">
@if(!empty($post->bg))
	<img src="{!! url($post->bg) !!}" width="100" height="100">
@endif	
</div>

{!! Form::hidden('type', $type , ['class' => 'form-control']) !!}

<div class="form-group col-sm-12">

{!! Form::label('bg', 'Icon:') !!}   
{!! Form::text('bg', null, ['class' => 'form-control', 'name'=>'bg', 'readonly'=>'readonly', 'placeholder'=>'Gunakan button "Select Image size min 81x81"']) !!}
    <a href="{!! route('fileupload') !!}" class="btn btn-success popup_selector form-control" data-inputid="bg">Select Image</a>
</div>

<div class="form-group col-sm-12">
@if(!empty($post->position_img))
	<img src="{!! url($post->position_img) !!}" width="100" height="100">
@endif	
</div>

<div class="form-group col-sm-12">
 {!! Form::label('position_img', 'Image:') !!}   
{!! Form::text('position_img', null, ['class' => 'form-control', 'name'=>'position_img', 'readonly'=>'readonly', 'placeholder'=>'Gunakan button "Select Image size min 145x137"']) !!}
    <a href="{!! route('fileupload') !!}" class="btn btn-success popup_selector form-control" data-inputid="bg">Select Image</a>    
</div>


<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('status', 'Status:') !!}<br/>
    {{ Form::radio('status', 1, isset($post) ? $post->status == 1 : false) }} Yes<br>
    {{ Form::radio('status', 0, isset($post) ? $post->status == 0 : true) }} No
</div>





<!-- else testimoni -->
@else
<!-- Title Field -->
<div class="form-group col-sm-12">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>
<!-- Content Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('content', 'Content:') !!}
    {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
</div>

{!! Form::hidden('type', $type , ['class' => 'form-control']) !!}
 <input type="hidden" name="parent" value="<?php echo date("m");?>">

<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('status', 'Status:') !!}<br/>
    {{ Form::radio('status', 1, isset($post) ? $post->status == 1 : false) }} Yes<br>
    {{ Form::radio('status', 0, isset($post) ? $post->status == 0 : true) }} No
</div>




<!-- end testimoni -->
@endif



<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){

    var maxField = 11; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    var x = 0; //Initial field counter is 1
    $(addButton).click(function(){ //Once add button is clicked
        if(x < maxField){ //Check maximum number of input fields
             //Increment field counter
             var fieldHTML ="<div style='width: 100%;float:left;'><div style='margin: -30px 19px 30px;' class'form-group col-sm-12'><label for='" + x + "'>Photo:</label></br> <input class='' type='file' id='photo" + x + "' name='photo" + x + "' placeholder=''/> <a href='javascript:void(0);' class='remove_button' title='Remove field'><i class='fa fa-times'><i/></a></div></div>";
            $(wrapper).append(fieldHTML); // Add field html
            $("#addPhoto").text("Add More Photo");
            x++;

        }
    });
    
   // var fieldHTML ="<div><div style='width: 1067px;margin: 0px auto;'' class'form-group col-sm-12'><label for='" + x + "'>Photo:</label></br><input class='form-control' type='text' id='photo" + x + "' name='photo" + x + "' placeholder='Gunakan button Select Image'/><a href='{!! route('fileupload') !!}' class='btn btn-success popup_selector form-control' data-inputid='photo" + x + "'>Select Image</a><a href='javascript:void(0);' class='remove_button' title='Remove field'><i class='fa fa-times'><i/></a></div></div>";
    //var fieldHTML = '<div><div style="width: 1067px;margin: 0px auto;" class"form-group col-sm-12"><label for="' + x + '">Photo:</label></br>{!! Form::text("photo'+ x +'", null, ["class" => "form-control", "name"=>"photo'+ x +'", "readonly"=>"readonly", "placeholder"=>"Gunakan button Select Image"]) !!}<a href="{!! route("fileupload") !!}" class="btn btn-success popup_selector form-control" data-inputid="photo'+ x +'">Select Image</a><a href="javascript:void(0);" class="remove_button" title="Remove field"><i class="fa fa-times"><i/></a></div></div>'; //New input field html 
    
    $(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
        if (x<=0) {
            $("#addPhoto").text("Add Photo");
        }
    });
});
</script>




<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! url('post?type='.$type) !!}" class="btn btn-default">Cancel</a>
</div>

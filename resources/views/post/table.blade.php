<div class="table-responsive">
<table class="table table-striped table-bordered table-hover" id="post-table">
    <thead>
        @if($type == 'slider')
        <!-- <th>Background</th>  -->
        <th>Photo</th>
        <th>Position</th> 
        @elseif($type == 'teknis')
        <th>Photo</th>
        
        @elseif($type == 'layanan')
        <th>Icon</th>
         <th>Photo</th>
          @elseif($type == 'personil')
        <th>Photo</th> 
        @elseif($type == 'testimonials')
        <th>Photo</th> 
         @elseif($type == 'berita')
        <th>Photo</th>
         <th>Position</th> 
        @elseif($type == 'manfaat') 
         <th>Photo</th>
        @endif
         @if($type == 'testimonials')
         <th>Name</th>
         <th>Employment</th>
         <th>Message</th> 
         
         @elseif($type == 'Massage')
         <th>Name</th>
         <th>Company</th>
         <th>Email</th>
         <th>Phone</th>
         <th>Subject</th>
         <th>Massage</th>
         @elseif($type == 'slider')
          <th>Title</th>
          @elseif($type == 'teknis')
          <th>Title</th>
          @elseif($type == 'personil')
           <th>Name</th>
           <th>Description</th>
          <th>Employment</th>
           <th>Position</th>
         @else
         <th>Title</th>
        <th>Content</th>
        @endif
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($post as $post)
        <tr>
             @if($type == 'slider')           
             <td><img width="50" height="50" src="{!! empty($post->bg) ? config('app.photo_default') : url($post->bg)  !!} "></td>
              <td>@if(!empty($post->position_text))Primary @else Secondary  @endif</td>
              @elseif($type == 'personil')
               <td><img width="50" height="50" src="{!! empty($post->bg) ? config('app.photo_default') : url($post->bg)  !!} "></td>
             @elseif($type == 'berita')
             <td><img width="50" height="50" src="{!! empty($post->bg) ? config('app.photo_default') : url($post->bg)  !!} "></td>
             <td>@if(!empty($post->position_text))Primary @else Secondary  @endif</td>
             @elseif($type == 'layanan')
             <td><img width="50" height="50" src="{!! empty($post->bg) ? config('app.photo_default') : url($post->bg)  !!} "></td>
              <td><img width="100" height="50" src="{!! empty($post->position_img) ? config('app.photo_default') : url($post->position_img)  !!} "></td>
             @elseif($type == 'testimonials')
               <td><img width="50" height="50" src="{!! empty($post->bg) ? config('app.photo_default') : url($post->bg)  !!} "></td>
             	<td>@if(!empty($post->position_text))Primary @else Secondary  @endif</td>
             @elseif($type == 'manfaat')
             <td><img width="50" height="50" src="{!! empty($post->bg) ? config('app.photo_default') : url($post->bg)  !!} "></td>
             @elseif($type == 'teknis')
              <td><img width="100" height="50" src="{!! empty($post->bg) ? config('app.photo_default') : url($post->bg)  !!} "></td>
              
             @endif
             @if($type == 'testimonials')
            <td>{!! $post->title !!}</td>
            <td>{!! $post->position_text !!}</td>
             <td>{!! $post->content !!}</td>
              @elseif($type == 'Massage')

              <td>{!! $post->keyword !!}</td>
                <td>{!! $post->position_img !!}</td>
                <td>{!! $post->bg !!}</td>
                <td>{!! $post->position_text !!}</td>
                <td>{!! $post->title !!}</td>
                <td>{!! $post->content !!}</td> 
              @elseif($type == 'slider')
  	 <td>{!! $post->title !!}</td>
               @elseif($type == 'personil')  
                  <td>{!! $post->title !!}</td>
                  <td>{!! $post->content !!}</td>
                  <td>{!! $post->keyword !!}</td>
                   <td>{!! $post->position_text !!}</td>
                @elseif($type == 'teknis')
                 <td>{!! $post->title !!}</td>   
            @else
            <td>{!! $post->title !!}</td>
            <td>{!! $post->content !!}</td>
            @endif
            <td>@if($post->status == '1')yes @else No @endif</td>
            <td>
                {!! Form::open(['route' => ['post.destroy', $post->id.''.'?type='.$type], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! url('post/'.$post->id.'/?type='.$type) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                     <a href="{!! url('post/'.$post->id.'/edit?type='.$type) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    @if($type == 'tag_line_supplier')
                    @elseif($type == 'tag_line_layanan')
                     @elseif($type == 'footer')
                     @elseif($type == 'join')
                      @elseif($type == 'home_profile')
                      @elseif($type == 'tag_line_manfaat')
                      @elseif($type == 'profile')
                      @elseif($type == 'teknis')
                    @else
                   
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    @endif

                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

</div>



<style type="text/css">
.red, i.icon.red, i.fa.red {
    color: #fd685c;
}
.small-icon, .small-icon-text, .small-icon-text h4, .small-icon-text p {
    clear: none;
}
.small-icon {
    display: block;
    float: left;
    font-size: 1.667em;
}
.small-icon, .big-icon {
    width: 2em;
    height: 2em;
    -webkit-border-radius: 25%;
    -moz-border-radius: 25%;
    border-radius: 25%;
    background-color: rgba(0,0,0,0.03);
    line-height: 2;
}
.icon {
    display: inline-block;
}
</style>
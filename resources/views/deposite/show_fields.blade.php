<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $deposite->id !!}</p>
</div>

<!-- Packet Id Field -->
<div class="form-group">
    {!! Form::label('packet_id', 'Packet Id:') !!}
    <p>{!! $deposite->packet_id !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $deposite->user_id !!}</p>
</div>

<!-- Denom Field -->
<div class="form-group">
    {!! Form::label('denom', 'Denom:') !!}
    <p>{!! $deposite->denom !!}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{!! $deposite->price !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $deposite->status !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $deposite->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $deposite->updated_at !!}</p>
</div>


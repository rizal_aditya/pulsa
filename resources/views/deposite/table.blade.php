<table class="table table-responsive" id="deposite-table">
    <thead>
        <th>Mitra</th>
        <th>Packet</th>
        <th>Denom</th>
        <th>Price</th>
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($deposite as $deposite)
        <tr>
            <td>{!! $deposite->user->name !!}</td>
            <td>{!! $deposite->packet->name !!}</td>
            <td>{!! $deposite->denom !!}</td>
            <td>{!! $deposite->price !!}</td>
            <td>
                  @if($deposite->status>0)
                <a href="{!! url('deposite/'.$deposite->id.'/disable') !!}">Disable</a>
             @else
                <a href="{!! url('deposite/'.$deposite->id.'/enable') !!}">Kirim Deposite</a>
             @endif
            </td>
            <td>
                {!! Form::open(['route' => ['deposite.destroy', $deposite->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('deposite.show', [$deposite->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                   <!--  <a href="{!! route('deposite.edit', [$deposite->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a> -->
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
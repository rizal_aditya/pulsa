@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Deposite
        </h1>
    </section>
    <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($deposite, ['route' => ['deposite.update', $deposite->id], 'method' => 'patch']) !!}

                        @include('deposite.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
    </div>
@endsection
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{!! config('app.title') !!} | Registration Teacher</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">


    <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/font-awesome/4.5.0/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/ionicons/2.0.1/css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/dist/css/skins/_all-skins.min.css') }}">
    <link rel="stylesheet"  href="{{ asset('/css/blue.css') }}">
    <script src="{{ asset('/js/jQuery-2.2.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.autocomplete.min.js') }}"></script>
    <!-- Bootstrap 3.3.6 -->
    

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition register-page">
<div class="register-search">
    <div class="register-logo">
        <a href="{{ url('/home') }}">{!! config('app.title') !!}</a>
    </div>

    <div class="register-box-body">
        <p class="login-box-msg">Search Client</p>
                        <?php 
                  
                        $client  = app('request')->input('access');
                        $data = base64_decode(strtr($client, '-_', '+/'));
                        $person = app('request')->input('account');
                        $person_id = base64_decode(strtr($person, '-_', '+/'));
                        $branch  = app('request')->input('branch');
                        $databranch = base64_decode(strtr($branch, '-_', '+/'));

                        ?>


           

            {!! csrf_field() !!}

            @if(empty($person_id))


                 @if(empty($data)) 
                   
                   @include('register.search-client')

                  @else

                     @include('register.search-name')
                      
                  @endif

              </br>
                        <!-- jika tidak kosong -->
                        @if(!empty($databranch))

                            @if($register->isEmpty())
                                <div class="text-center">Teacher Not found to person.  <a href="register/show?access={!! $client !!}">Register</a></div>
                            @else
                                 @include('register.table')

                            {{ $register->links() }}
                            
                            @endif  
                       
                        @endif
                        

           @else
           

            




           @endif       
            

       
    </div>
    <!-- /.form-box -->
</div>
<!-- /.register-box -->


<script  src="{{ asset('/js/jquery.autocomplete.min.js') }}"></script>
<script src="{{ asset('/js/bootstrap.min.js') }}"></script>
<script  src="{{ asset('/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('/js/icheck.min.js') }}"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>


</body>
</html>

		@include('frontend.header')
        @include('frontend.page')
        <main role="main">
        	<div id="intro-wrap" class="">
				<div id="intro" class="preload darken more-button">					
										
				</div><!-- intro -->
			</div><!-- intro-wrap -->

        	<div id="main">	
        	<section class="row section" style="background-color: rgb(255, 255, 255);background-image: url('img/loop3.png');">
					<div class="row-content buffer even clear-after" >
						<div class="section-title"><h3>Lengkapi Isian Formulir Sekolahmu</h3></div>	
						<p>Aplikasi sekolah berbasis mobile apps garapan PT. Layana Computindo merupakan wujud dari kemajuan teknologi informasi khususnya di bidang pendidikan.</p>
						<div class="column seven">
							@include('adminlte-templates::common.errors')
							  @include('flash::message')
							
							 {!! Form::open(['route' => 'formRegister.store','id'=>'contact-form','class'=>'contact-section']) !!}	

								 @include('register.fields')
							
							{!! Form::close() !!}
							<div id="success"></div>
						</div>
					</div>
				</section>
			 </div><!-- id-main -->
        </main><!-- main -->	

<form method="post" action="{{ url('/register/search') }}">
 <div class="box-footer ">
  <div class="form-group has-feedback {{ $errors->has('client_id') ? ' has-error' : '' }}">
    
  <input type="hidden" value="{!! $data !!}" name="client_id">

  </div> 


  @if(!empty($databranch))

  @foreach(App\Models\Person::where(['branch_id'=>$databranch,'type'=>'teacher'])->get() as $rows)
  <?php $br = "".$rows->branch_id.""; ?>
  @endforeach


  @endif
      <div class="form-group has-feedback {{ $errors->has('branch_id') ? ' has-error' : '' }}"> 
      
      @if(!empty($databranch))
           <select name="branch_id" class="form-control selectpicker"  data-live-search="true">
               <option value="">Select Branch</option>
           @foreach(App\Models\Branch::where('client_id', $data)->get() as $row)
              @if($br == $row->id)
              <option value="{!! $row->id !!}" selected>{!! $row->name !!}</option>
              @else
               <option value="{!! $row->id !!}" >{!! $row->name !!}</option>
              @endif
           @endforeach 
           </select> 

       @else
          <select name="branch_id" class="form-control selectpicker">
           @foreach(App\Models\Branch::where('client_id', $data)->get() as $row)
            <option value="{!! $row->id !!}">{!! $row->name !!}</option>
           @endforeach 
           </select> 


       @endif      

     </div>




      <div class="input-group form-group has-feedback {{ $errors->has('name') ? ' has-error' : '' }}">
        <input name="name" placeholder="Search Name Teacher ..." class="form-control" type="text">
        <span class="input-group-btn">
          <button type="submit" class="btn btn-primary">Search</button>
        </span>
      </div>
</div>

  
  
  </form> 
<span class="pre-input"><i class="icon icon-home"></i></span>
{!! Form::text('name', null, ['class' => 'name plain buffer','placeholder'=>'Nama Sekolah','required'=>'']) !!}
<input type="hidden" value="school" name="type">
 {!! Form::hidden('role_id', App\Role::where('name','user')->first()->id, ['class' => 'form-control']) !!}
<span class="pre-input"><i class="icon icon-user"></i></span>
{!! Form::text('owner', null, ['class' => 'name plain buffer','placeholder'=>'Nama Pendaftar','required'=>'']) !!}
<span class="pre-input"><i class="icon icon-email"></i></span>
{!! Form::email('email', null, ['class' => 'name plain buffer','placeholder'=>'Alamat Email','required'=>'']) !!}
<span class="pre-input"><i class="icon icon-smartphone"></i></span>
{!! Form::text('phone', null, ['class' => 'name plain buffer','placeholder'=>'No. Telpon Sekolah','required'=>'']) !!}
{!! Form::textarea('address', null, ['class' => 'plain buffer','placeholder'=>'Alamat Sekolah']) !!}
{!! Form::submit('Kirim Registrasi', ['class' => 'plain button red','id'=>'send']) !!}

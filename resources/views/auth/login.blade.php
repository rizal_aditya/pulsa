<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{!! config('app.title') !!}</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- Bootstrap 3.3.6 -->
   <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">
   <link rel="stylesheet" href="{{ asset('/font-awesome/4.5.0/css/font-awesome.min.css') }}">
   <link rel="stylesheet" href="{{ asset('/ionicons/2.0.1/css/ionicons.min.css') }}">
   <link rel="stylesheet" href="{{ asset('/dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/bootstrap-select.min.css') }}">
   <link rel="stylesheet" href="{{ asset('/dist/css/skins/_all-skins.min.css') }}">
   <link rel="stylesheet"  href="{{ asset('/css/blue.css') }}">
    <script src="{{ asset('/js/jQuery-2.2.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.autocomplete.min.js') }}"></script>

     
   <!-- <link rel="stylesheet" href="{{ asset('/css/select2.min.css') }}"> -->
  
    
  

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="{{ url('/home') }}">{!! config('app.title') !!}</a>
    </div>

    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>

        <div id="searchfield">
        


       <form id="searchform" method="post" action="{{ url('/login') }}">
            {!! csrf_field() !!}
          
              
                <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                    <input  type="email"  class="form-control" name="email" value="" placeholder="Email">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                     <!-- <div id="suggestions"></div> -->
                  

                    @if ($errors->has('email'))
                        <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

             
                </div>

                   
           

            <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                <input type="password" class="form-control" placeholder="Password" name="password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @if ($errors->has('password'))
                    <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif

            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox" name="remember"> Remember Me
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
                <!-- /.col -->
            </div>
        </form>

   


  <style type="text/css">


/* SEARCHRESULTS */
#searchresults {
    border: 1px solid #d5d2d2;
    background-color: #f9f9f9;
    font-size: 10px;
    line-height: 14px;
    z-index: 1000;
    position: absolute;
    border-radius: 3px;
    margin: 50px 0px 0px 0px;
    width: 320px;
}

#searchresults .positon{
  margin: 0px auto;
  width: 300px;


}

#searchresults .positon ul{
    float: left;
    width: 100%;
    margin: none;
    padding: 0px;
    list-style: none;
}

#searchresults .positon ul li{
float: left;
margin: 5px 0px 5px 0px;
width: 100%;
cursor: pointer;
border-bottom: 1px solid #ddd;
  }

  #searchresults .positon ul li img{
    float: left;
    padding: 0px 0px 3px 0px;

  }

#searchresults .positon ul li .searchheading{
float: left;
width: auto;
font-size: 12px;
font-weight: bold;
padding: 0px 6px;
width: 250px;


}
#searchresults .positon ul li .emaile{
float: left;
width: auto;
font-size: 10px;
padding: 0px 6px;
width: 250px;
}

.autocomplete-suggestions { border: 1px solid #999; background: #fff; cursor: default; overflow: auto; }
.autocomplete-suggestion { padding: 10px 5px; font-size: 1.2em; white-space: nowrap; overflow: hidden; }
.autocomplete-selected { background: #f0f0f0; }
.autocomplete-suggestions strong { font-weight: normal; color: #3399ff; }
  </style>

  </div><!-- @end #searchfield -->


        <a href="{{ url('/password/reset') }}">I forgot my password</a><br>
        <a href="{{ url('/register') }}" class="text-center">Register a new membership</a>

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->



<script  src="{{ asset('/js/jquery.autocomplete.min.js') }}"></script>
<script src="{{ asset('/js/bootstrap.min.js') }}"></script>
<script  src="{{ asset('/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('/js/icheck.min.js') }}"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
<script>
$(function(){
  var currencies = 
  [

    @foreach (App\User::join('role_user', 'users.id', '=', 'role_user.user_id')->get() as $user)
    
    { 
      value: '{{ $user->email }} ', 
      data: '{{ $user->client_id }}', 
    
      name:'{{ $user->name }}', 
    },  
        
    @endforeach

  ];

  
  // setup autocomplete function pulling from currencies[] array
  $('#autocomplete').autocomplete(
      {
        lookup: currencies,
        onSelect: function (suggestion) 
        {
           var msg = '<input type="hidden" name="client_id" value="' + suggestion.data + '"> <input type="hidden" name="name" value="' + suggestion.name + '" disabled>';
           $('#outputcontent').html(msg);
           // var email = ''+ suggestion.value +'';

           // $.ajax({
           //    type :"GET",
           //    url :"{{ url('user/login') }}",
           //    data : email,
           //    success : function()
           //    {
                
           //      $("#autocomplete").html(msg);

           //    },
           //    error : function()
           //    {
           //      alert("Gagal");
           //    }
           //  });
        }
      }
  );
  

});
</script>
</body>
</html>

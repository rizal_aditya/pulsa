<!-- [A[Btransaction Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('[A[Btransaction_id', '[A[Btransaction Id:') !!}
    {!! Form::number('[A[Btransaction_id', null, ['class' => 'form-control']) !!}
</div>

<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- [A[B[A[Bpulsa Integer[D[D[D[D[C Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('[A[B[A[Bpulsa_integer[D[D[D[D[C_id', '[A[B[A[Bpulsa Integer[D[D[D[D[C Id:') !!}
    {!! Form::number('[A[B[A[Bpulsa_integer[D[D[D[D[C_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'Price:') !!}
    {!! Form::number('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', 'Phone:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::number('status', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('history.index') !!}" class="btn btn-default">Cancel</a>
</div>

<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $history->id !!}</p>
</div>

<!-- [A[Btransaction Id Field -->
<div class="form-group">
    {!! Form::label('[A[Btransaction_id', '[A[Btransaction Id:') !!}
    <p>{!! $history->[A[Btransaction_id !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $history->user_id !!}</p>
</div>

<!-- [A[B[A[Bpulsa Integer[D[D[D[D[C Id Field -->
<div class="form-group">
    {!! Form::label('[A[B[A[Bpulsa_integer[D[D[D[D[C_id', '[A[B[A[Bpulsa Integer[D[D[D[D[C Id:') !!}
    <p>{!! $history->[A[B[A[Bpulsa_integer[D[D[D[D[C_id !!}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{!! $history->price !!}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{!! $history->phone !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $history->status !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $history->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $history->updated_at !!}</p>
</div>


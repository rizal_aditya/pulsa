<table class="table table-responsive" id="supplier-table">
    <thead>
        <th>Photo</th>
        <th>Name</th>
        <th>Phone</th>
        <th>Address</th>
        <th>Description</th>
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($supplier as $supplier)
        <tr>
             <td><img width="50" height="50" src="{!! empty($supplier->photo) ? config('app.photo_default') : url($supplier->photo)  !!} "></td>
            <td>{!! $supplier->name !!}</td>
            <td>{!! $supplier->phone !!}</td>
            <td>{!! $supplier->address !!}</td>
            <td>{!! $supplier->description !!}</td>
            <td>@if($supplier->status == '1')yes @else No @endif</td>
            <td>
                {!! Form::open(['route' => ['supplier.destroy', $supplier->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('supplier.show', [$supplier->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('supplier.edit', [$supplier->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
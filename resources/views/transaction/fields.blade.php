<!-- User Id Field -->
@role('user') 
<div class="form-group col-sm-12">
    {!! Form::hidden('user_id', Auth::user()->id, ['class' => 'form-control']) !!}
    
        
</div>
@endrole
@role('admin') 
<div class="form-group col-sm-12">
    {!! Form::label('user_id', 'Mitra:') !!}
    {{ Form::select('user_id', App\User::where('type','mitra')->get()->lists('name', 'id'), null, ['class' => 'form-control selectpicker', 'data-live-search'=>'true']) }}
     
</div>
@endrole


<!-- Voucher Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('code', 'Code Voucher:') !!}
     {{ Form::select('code', App\Models\Voucher::get()->lists('typeName', 'code'), null, ['class' => 'form-control selectpicker', 'data-live-search'=>'true']) }}
     
</div>

<!-- Phone Field -->
<div class="form-group col-sm-12">
    {!! Form::label('phone', 'Phone:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('status', 'Status:') !!}<br/>
    {{ Form::radio('status', 1, isset($post) ? $post->status == 1 : false) }} Yes<br>
    {{ Form::radio('status', 0, isset($post) ? $post->status == 0 : true) }} No
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('transaction.index') !!}" class="btn btn-default">Cancel</a>
</div>

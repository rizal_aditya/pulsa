<table class="table table-responsive" id="transaction-table">
    <thead>
        <th>Mitra</th>
        <th>Voucher</th>
        <th>Phone</th>
        <th>Purchase</th>
        <th>Selling</th>
        <th>Result</th>
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($transaction as $transaction)
        <tr>
            <td>{!! $transaction->user->name !!}</td>
            <td>{!! $transaction->voucher->name !!} - {!! $transaction->voucher->denom !!} </td>
            <td>{!! $transaction->phone !!}</td>
            <td>{!! $transaction->purchase !!}</td>
            <td>{!! $transaction->selling !!}</td>
            <td>{!! $transaction->phone !!}</td>
            <td>{!! $transaction->result !!}</td>
            <td>{!! $transaction->status !!}</td>
            <td>
                {!! Form::open(['route' => ['transaction.destroy', $transaction->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('transaction.show', [$transaction->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('transaction.edit', [$transaction->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
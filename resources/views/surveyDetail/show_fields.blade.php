<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $surveyDetail->id !!}</p>
</div>

<!-- Survey Id Field -->
<div class="form-group">
    {!! Form::label('survey_id', 'Survey Id:') !!}
    <p>{!! $surveyDetail->survey_id !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $surveyDetail->user_id !!}</p>
</div>

<!-- Essay Field -->
<div class="form-group">
    {!! Form::label('essay', 'Essay:') !!}
    <p>{!! $surveyDetail->essay !!}</p>
</div>

<!-- Selection Field -->
<div class="form-group">
    {!! Form::label('selection', 'Selection:') !!}
    <p>{!! $surveyDetail->selection !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $surveyDetail->status !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $surveyDetail->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $surveyDetail->updated_at !!}</p>
</div>


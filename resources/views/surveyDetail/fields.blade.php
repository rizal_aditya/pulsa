<!-- Survey Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('survey_id', 'Survey Id:') !!}
    {!! Form::number('survey_id', null, ['class' => 'form-control']) !!}
</div>

<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Essay Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('essay', 'Essay:') !!}
    {!! Form::textarea('essay', null, ['class' => 'form-control']) !!}
</div>

<!-- Selection Field -->
<div class="form-group col-sm-6">
    {!! Form::label('selection', 'Selection:') !!}
    {!! Form::number('selection', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::number('status', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('surveyDetail.index') !!}" class="btn btn-default">Cancel</a>
</div>

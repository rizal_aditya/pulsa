<table class="table table-responsive" id="surveyDetail-table">
    <thead>
        <th>Survey Id</th>
        <th>User Id</th>
        <th>Essay</th>
        <th>Selection</th>
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($surveyDetail as $surveyDetail)
        <tr>
            <td>{!! $surveyDetail->survey_id !!}</td>
            <td>{!! $surveyDetail->user_id !!}</td>
            <td>{!! $surveyDetail->essay !!}</td>
            <td>{!! $surveyDetail->selection !!}</td>
            <td>{!! $surveyDetail->status !!}</td>
            <td>
                {!! Form::open(['route' => ['surveyDetail.destroy', $surveyDetail->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('surveyDetail.show', [$surveyDetail->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('surveyDetail.edit', [$surveyDetail->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Menu
          <small>{{ $type }}</small>
        </h1>
    </section>
    <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($menu, ['route' => ['menu.update', $menu->id], 'method' => 'patch']) !!}

                        @include('menu.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
    </div>
@endsection
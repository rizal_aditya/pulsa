@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Postfile
        </h1>
    </section>
    <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($postfile, ['route' => ['postfile.update', $postfile->id], 'method' => 'patch']) !!}

                        @include('postfile.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
    </div>
@endsection
<div class="table-responsive">
<table class="table table-striped table-bordered table-hover" id="client-table">
    <thead>
        <tr>
        <th>No</th>
	<th>Register ID</th>
        <th>Name</th>
        <th>Owner</th>    
        <th>Manager</th>
        <th>Phone Office</th>
        <th>Address</th>
        <th>Tgl Register</th>
       
    </tr>
     </thead>
               
    <tbody>
    <?php $i=1; if(!empty($client)){ foreach($client as $client){ ?>
  
        <tr>
         <td><?php echo $i;?></td>
		 <td>{!! $client->id !!}</td>
            <td>{!! $client->name !!}</td>
            <td>{!! $client->owner !!}</td>
            
            <td>{!! $client->manager !!}</td>
            <td>@if($client->phone =="0") @else {!! $client->phone !!} @endif</td>
            <td>{!! $client->address !!}</td>
            <td>{!! $client->created_at->format('d F Y') !!}
               
            </td>
           
        </tr>      
    <?php $i++; } } ?>
    </tbody>
</table>
</div>

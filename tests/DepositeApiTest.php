<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DepositeApiTest extends TestCase
{
    use MakeDepositeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateDeposite()
    {
        $deposite = $this->fakeDepositeData();
        $this->json('POST', '/api/v1/deposite', $deposite);

        $this->assertApiResponse($deposite);
    }

    /**
     * @test
     */
    public function testReadDeposite()
    {
        $deposite = $this->makeDeposite();
        $this->json('GET', '/api/v1/deposite/'.$deposite->id);

        $this->assertApiResponse($deposite->toArray());
    }

    /**
     * @test
     */
    public function testUpdateDeposite()
    {
        $deposite = $this->makeDeposite();
        $editedDeposite = $this->fakeDepositeData();

        $this->json('PUT', '/api/v1/deposite/'.$deposite->id, $editedDeposite);

        $this->assertApiResponse($editedDeposite);
    }

    /**
     * @test
     */
    public function testDeleteDeposite()
    {
        $deposite = $this->makeDeposite();
        $this->json('DELETE', '/api/v1/deposite/'.$deposite->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/deposite/'.$deposite->id);

        $this->assertResponseStatus(404);
    }
}

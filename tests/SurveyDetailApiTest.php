<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SurveyDetailApiTest extends TestCase
{
    use MakeSurveyDetailTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSurveyDetail()
    {
        $surveyDetail = $this->fakeSurveyDetailData();
        $this->json('POST', '/api/v1/surveyDetail', $surveyDetail);

        $this->assertApiResponse($surveyDetail);
    }

    /**
     * @test
     */
    public function testReadSurveyDetail()
    {
        $surveyDetail = $this->makeSurveyDetail();
        $this->json('GET', '/api/v1/surveyDetail/'.$surveyDetail->id);

        $this->assertApiResponse($surveyDetail->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSurveyDetail()
    {
        $surveyDetail = $this->makeSurveyDetail();
        $editedSurveyDetail = $this->fakeSurveyDetailData();

        $this->json('PUT', '/api/v1/surveyDetail/'.$surveyDetail->id, $editedSurveyDetail);

        $this->assertApiResponse($editedSurveyDetail);
    }

    /**
     * @test
     */
    public function testDeleteSurveyDetail()
    {
        $surveyDetail = $this->makeSurveyDetail();
        $this->json('DELETE', '/api/v1/surveyDetail/'.$surveyDetail->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/surveyDetail/'.$surveyDetail->id);

        $this->assertResponseStatus(404);
    }
}

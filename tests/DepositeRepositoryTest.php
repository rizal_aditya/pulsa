<?php

use App\Models\Deposite;
use App\Repositories\DepositeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DepositeRepositoryTest extends TestCase
{
    use MakeDepositeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var DepositeRepository
     */
    protected $depositeRepo;

    public function setUp()
    {
        parent::setUp();
        $this->depositeRepo = App::make(DepositeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateDeposite()
    {
        $deposite = $this->fakeDepositeData();
        $createdDeposite = $this->depositeRepo->create($deposite);
        $createdDeposite = $createdDeposite->toArray();
        $this->assertArrayHasKey('id', $createdDeposite);
        $this->assertNotNull($createdDeposite['id'], 'Created Deposite must have id specified');
        $this->assertNotNull(Deposite::find($createdDeposite['id']), 'Deposite with given id must be in DB');
        $this->assertModelData($deposite, $createdDeposite);
    }

    /**
     * @test read
     */
    public function testReadDeposite()
    {
        $deposite = $this->makeDeposite();
        $dbDeposite = $this->depositeRepo->find($deposite->id);
        $dbDeposite = $dbDeposite->toArray();
        $this->assertModelData($deposite->toArray(), $dbDeposite);
    }

    /**
     * @test update
     */
    public function testUpdateDeposite()
    {
        $deposite = $this->makeDeposite();
        $fakeDeposite = $this->fakeDepositeData();
        $updatedDeposite = $this->depositeRepo->update($fakeDeposite, $deposite->id);
        $this->assertModelData($fakeDeposite, $updatedDeposite->toArray());
        $dbDeposite = $this->depositeRepo->find($deposite->id);
        $this->assertModelData($fakeDeposite, $dbDeposite->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteDeposite()
    {
        $deposite = $this->makeDeposite();
        $resp = $this->depositeRepo->delete($deposite->id);
        $this->assertTrue($resp);
        $this->assertNull(Deposite::find($deposite->id), 'Deposite should not exist in DB');
    }
}

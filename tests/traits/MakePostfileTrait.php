<?php

use Faker\Factory as Faker;
use App\Models\Postfile;
use App\Repositories\PostfileRepository;

trait MakePostfileTrait
{
    /**
     * Create fake instance of Postfile and save it in database
     *
     * @param array $postfileFields
     * @return Postfile
     */
    public function makePostfile($postfileFields = [])
    {
        /** @var PostfileRepository $postfileRepo */
        $postfileRepo = App::make(PostfileRepository::class);
        $theme = $this->fakePostfileData($postfileFields);
        return $postfileRepo->create($theme);
    }

    /**
     * Get fake instance of Postfile
     *
     * @param array $postfileFields
     * @return Postfile
     */
    public function fakePostfile($postfileFields = [])
    {
        return new Postfile($this->fakePostfileData($postfileFields));
    }

    /**
     * Get fake data of Postfile
     *
     * @param array $postFields
     * @return array
     */
    public function fakePostfileData($postfileFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'post_id' => $fake->randomDigitNotNull,
            'branch_id' => $fake->randomDigitNotNull,
            'type' => $fake->word,
            'name' => $fake->word,
            'content' => $fake->text,
            'file' => $fake->text,
            'status' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $postfileFields);
    }
}

<?php

use Faker\Factory as Faker;
use App\Models\SurveyDetail;
use App\Repositories\SurveyDetailRepository;

trait MakeSurveyDetailTrait
{
    /**
     * Create fake instance of SurveyDetail and save it in database
     *
     * @param array $surveyDetailFields
     * @return SurveyDetail
     */
    public function makeSurveyDetail($surveyDetailFields = [])
    {
        /** @var SurveyDetailRepository $surveyDetailRepo */
        $surveyDetailRepo = App::make(SurveyDetailRepository::class);
        $theme = $this->fakeSurveyDetailData($surveyDetailFields);
        return $surveyDetailRepo->create($theme);
    }

    /**
     * Get fake instance of SurveyDetail
     *
     * @param array $surveyDetailFields
     * @return SurveyDetail
     */
    public function fakeSurveyDetail($surveyDetailFields = [])
    {
        return new SurveyDetail($this->fakeSurveyDetailData($surveyDetailFields));
    }

    /**
     * Get fake data of SurveyDetail
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSurveyDetailData($surveyDetailFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'survey_id' => $fake->randomDigitNotNull,
            'user_id' => $fake->randomDigitNotNull,
            'essay' => $fake->text,
            'selection' => $fake->randomDigitNotNull,
            'status' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $surveyDetailFields);
    }
}

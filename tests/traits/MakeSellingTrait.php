<?php

use Faker\Factory as Faker;
use App\Models\Selling;
use App\Repositories\SellingRepository;

trait MakeSellingTrait
{
    /**
     * Create fake instance of Selling and save it in database
     *
     * @param array $sellingFields
     * @return Selling
     */
    public function makeSelling($sellingFields = [])
    {
        /** @var SellingRepository $sellingRepo */
        $sellingRepo = App::make(SellingRepository::class);
        $theme = $this->fakeSellingData($sellingFields);
        return $sellingRepo->create($theme);
    }

    /**
     * Get fake instance of Selling
     *
     * @param array $sellingFields
     * @return Selling
     */
    public function fakeSelling($sellingFields = [])
    {
        return new Selling($this->fakeSellingData($sellingFields));
    }

    /**
     * Get fake data of Selling
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSellingData($sellingFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'price' => $fake->randomDigitNotNull,
            'status' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $sellingFields);
    }
}

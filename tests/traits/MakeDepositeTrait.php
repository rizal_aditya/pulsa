<?php

use Faker\Factory as Faker;
use App\Models\Deposite;
use App\Repositories\DepositeRepository;

trait MakeDepositeTrait
{
    /**
     * Create fake instance of Deposite and save it in database
     *
     * @param array $depositeFields
     * @return Deposite
     */
    public function makeDeposite($depositeFields = [])
    {
        /** @var DepositeRepository $depositeRepo */
        $depositeRepo = App::make(DepositeRepository::class);
        $theme = $this->fakeDepositeData($depositeFields);
        return $depositeRepo->create($theme);
    }

    /**
     * Get fake instance of Deposite
     *
     * @param array $depositeFields
     * @return Deposite
     */
    public function fakeDeposite($depositeFields = [])
    {
        return new Deposite($this->fakeDepositeData($depositeFields));
    }

    /**
     * Get fake data of Deposite
     *
     * @param array $postFields
     * @return array
     */
    public function fakeDepositeData($depositeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'packet_id' => $fake->randomDigitNotNull,
            'user_id' => $fake->randomDigitNotNull,
            'denom' => $fake->randomDigitNotNull,
            'price' => $fake->randomDigitNotNull,
            'status' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $depositeFields);
    }
}

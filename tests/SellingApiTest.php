<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SellingApiTest extends TestCase
{
    use MakeSellingTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSelling()
    {
        $selling = $this->fakeSellingData();
        $this->json('POST', '/api/v1/selling', $selling);

        $this->assertApiResponse($selling);
    }

    /**
     * @test
     */
    public function testReadSelling()
    {
        $selling = $this->makeSelling();
        $this->json('GET', '/api/v1/selling/'.$selling->id);

        $this->assertApiResponse($selling->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSelling()
    {
        $selling = $this->makeSelling();
        $editedSelling = $this->fakeSellingData();

        $this->json('PUT', '/api/v1/selling/'.$selling->id, $editedSelling);

        $this->assertApiResponse($editedSelling);
    }

    /**
     * @test
     */
    public function testDeleteSelling()
    {
        $selling = $this->makeSelling();
        $this->json('DELETE', '/api/v1/selling/'.$selling->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/selling/'.$selling->id);

        $this->assertResponseStatus(404);
    }
}

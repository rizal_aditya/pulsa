<?php

use App\Models\Selling;
use App\Repositories\SellingRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SellingRepositoryTest extends TestCase
{
    use MakeSellingTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var SellingRepository
     */
    protected $sellingRepo;

    public function setUp()
    {
        parent::setUp();
        $this->sellingRepo = App::make(SellingRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSelling()
    {
        $selling = $this->fakeSellingData();
        $createdSelling = $this->sellingRepo->create($selling);
        $createdSelling = $createdSelling->toArray();
        $this->assertArrayHasKey('id', $createdSelling);
        $this->assertNotNull($createdSelling['id'], 'Created Selling must have id specified');
        $this->assertNotNull(Selling::find($createdSelling['id']), 'Selling with given id must be in DB');
        $this->assertModelData($selling, $createdSelling);
    }

    /**
     * @test read
     */
    public function testReadSelling()
    {
        $selling = $this->makeSelling();
        $dbSelling = $this->sellingRepo->find($selling->id);
        $dbSelling = $dbSelling->toArray();
        $this->assertModelData($selling->toArray(), $dbSelling);
    }

    /**
     * @test update
     */
    public function testUpdateSelling()
    {
        $selling = $this->makeSelling();
        $fakeSelling = $this->fakeSellingData();
        $updatedSelling = $this->sellingRepo->update($fakeSelling, $selling->id);
        $this->assertModelData($fakeSelling, $updatedSelling->toArray());
        $dbSelling = $this->sellingRepo->find($selling->id);
        $this->assertModelData($fakeSelling, $dbSelling->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSelling()
    {
        $selling = $this->makeSelling();
        $resp = $this->sellingRepo->delete($selling->id);
        $this->assertTrue($resp);
        $this->assertNull(Selling::find($selling->id), 'Selling should not exist in DB');
    }
}
